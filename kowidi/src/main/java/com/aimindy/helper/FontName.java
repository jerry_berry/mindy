package com.aimindy.helper;

/** Fonts names
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 16.12.2015.
 */
public class FontName {
    public static final String ROBOTO_SLAB_REGULAR = "RobotoSlab-Regular";
    public static final String ROBOTO_SLAB_THIN = "RobotoSlab-Thin";
    public static final String ROBOTO_SLAB_LIGHT = "RobotoSlab-Light";
    public static final String ROBOTO_SLAB_BOLD = "RobotoSlab-Bold";
}
