package com.aimindy.helper;

import android.support.design.widget.Snackbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.aimindy.AppConstants;
import com.aimindy.R;

/** Show snackbar notifications
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 21.09.2015.
 */
@SuppressWarnings("unused")
public class NotificationHelper {
    /**
     * Show error notification
     * @param view - any view on the layout
     * @param resId - string resource id
     */
    public static void showError(View view,int resId){
        if (view==null)
            return;
        try{
            Snackbar bar = Snackbar.make(view, resId, Snackbar.LENGTH_LONG);
            addFunctions(bar);
            bar.getView().setBackgroundColor(view.getResources().getColor(R.color.bar_error));
            bar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Show error notification
     * @param view - any view on the layout
     * @param error - string error message
     */

    public static void showError(View view,String error){
        if (view==null)
            return;

        try {
            Snackbar bar = Snackbar.make(view, error, Snackbar.LENGTH_LONG);
            addFunctions(bar);
            bar.getView().setBackgroundColor(view.getResources().getColor(R.color.bar_error));
            bar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show Html formatted error notification
     * @param view any view on the layout
     * @param error notification text
     */
    public static void showErrorHtml(View view,String error){
        if (view==null)
            return;

        try {
            Snackbar bar = Snackbar.make(view, Html.fromHtml(error), Snackbar.LENGTH_LONG);
            View rootView = bar.getView();
            rootView.setBackgroundColor(view.getResources().getColor(R.color.bar_error));
            addFunctions(bar);
            bar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show success snackbar
     * @param view any view on the layout
     * @param resId resource id with success message
     */
    public static void showSuccess(View view, int resId) {
        if (view==null)
            return;

        try{
            Snackbar bar = Snackbar.make(view, resId, Snackbar.LENGTH_LONG);
            bar.getView().setBackgroundColor(view.getResources().getColor(R.color.bar_success));
            addFunctions(bar);
            bar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Show success snackbar
     * @param view any view on the layout
     * @param message string message to show
     */
    public static void showSuccess(View view, String message) {
        if (view==null)
            return;

        try{
            Snackbar bar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            bar.getView().setBackgroundColor(view.getResources().getColor(R.color.bar_success));
            addFunctions(bar);
            bar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Show success snackbar
     * @param view any view on the layout
     * @param message string message to show
     */
    public static void showSuccessHtml(View view, String message) {
        if (view==null)
            return;

        try{
            Snackbar bar = Snackbar.make(view, Html.fromHtml(message), Snackbar.LENGTH_LONG);
            View rootView = bar.getView();
            rootView.setBackgroundColor(view.getResources().getColor(R.color.bar_success));
            addFunctions(bar);
            bar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void showErrorNoInternet(View v) {
        showError(v,R.string.error_no_internet);
    }
    private static void addFunctions(final Snackbar bar){
        View rootView = bar.getView();
        TextView tv = (TextView)rootView.findViewById(android.support.design.R.id.snackbar_text);
        if (tv!=null)
            tv.setMaxLines(AppConstants.SNACKBAR_MAX_HEIGHT);
    }
}
