package com.aimindy.helper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.util.Log;

/**Class for detect is debug or release apk ran
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 06.03.2015.
 */
public class DebugDetect {
    private static boolean mIsChecked = false;
    private static boolean mIsDebug = false;

    /**
     * Is debug apk ran
     * @param context Context
     * @return true for debug apk
     */
    public static boolean isDebug(Context context){
        if (mIsChecked)
            return mIsDebug;
        if (context==null)
            return mIsDebug;
        ApplicationInfo info = context.getApplicationInfo();
        if (info!=null){
            mIsChecked = true;
            if ((info.flags & ApplicationInfo.FLAG_DEBUGGABLE) !=0){
                mIsDebug = true;
            }
            else{
                mIsDebug = false;
            }
        }
        return mIsDebug;
    }
    public static void debugIntent(String tag,Intent intent) {
        Log.v(tag, "action: " + intent.getAction());
        Log.v(tag, "component: " + intent.getComponent());
        Bundle extras = intent.getExtras();
        if (extras != null) {
            for (String key: extras.keySet()) {
                Log.v(tag, "key [" + key + "]: " +
                        extras.get(key));
            }
        }
        else {
            Log.v(tag, "no extras");
        }
    }
}
