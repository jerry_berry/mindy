package com.aimindy.helper;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import com.aimindy.KowidiApp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/** Class cahced fonts from assets folder
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 16.12.2015.
 */
public class FontCache {

    private static String TAG = "FontCache";
    private static final String FONT_DIR = "fonts";
    private static Map<String, Typeface> cache = new HashMap<>();
    private static Map<String, String> fontMapping = new HashMap<>();
    private static FontCache instance;

    public static FontCache getInstance() {
        if (instance == null) {
            instance = new FontCache();
        }
        return instance;
    }

    /**
     * add font to cache
     * @param name font name
     * @param fontFilename name of the file
     */
    public void addFont(String name, String fontFilename) {
        fontMapping.put(name, fontFilename);
    }

    private FontCache() {
        if (KowidiApp.getContext()==null)
            return;
        AssetManager am = KowidiApp.getContext().getResources().getAssets();
        String fileList[];
        try {
            fileList = am.list(FONT_DIR);
        } catch (IOException e) {
            Log.e(TAG, "Error loading fonts from assets/fonts.");
            return;
        }

        for (String filename : fileList) {
            String alias = filename.substring(0, filename.lastIndexOf('.'));
            fontMapping.put(alias, filename);
            fontMapping.put(alias.toLowerCase(), filename);
        }
    }

    /**
     * Get font by name
     * @param fontName font name
     * @return typeface with this font or default
     */
    public Typeface get(String fontName) {
        String fontFilename = fontMapping.get(fontName);
        if (fontFilename == null) {
            Log.e(TAG, "Couldn't find font " + fontName + ". Maybe you need to call addFont() first?");
            return null;
        }
        if (cache.containsKey(fontName)) {
            return cache.get(fontName);
        } else {
            if (KowidiApp.getContext()==null)
                return Typeface.DEFAULT;
            Typeface typeface = Typeface.createFromAsset(KowidiApp.getContext().getAssets(), FONT_DIR + "/" + fontFilename);
            cache.put(fontFilename, typeface);
            return typeface;
        }
    }
}