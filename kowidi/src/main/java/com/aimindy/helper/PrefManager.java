package com.aimindy.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by android on 1/19/2017.
 */

public class PrefManager {

    static SharedPreferences pref;
    static SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "mindy-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String NAME = "name";
    private static final String accessToken = "accessToken";



    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setName(String name) {
        editor.putString(NAME, name);
        editor.commit();
    }

    public String getName() {
        return pref.getString(NAME,"");
    }

    public  void setAccessToken(String accessToken) {
        editor.putString(accessToken, accessToken);
        editor.commit();    }

    public  String getAccessToken() {
        return   pref.getString(accessToken,"");

    }
}
