package com.aimindy.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aimindy.ui.ItemsFragment;
import com.aimindy.ui.fragment.BlankFragment;
import com.aimindy.ui.fragment.SettingFragment;

/**
 * Date: 02.12.16
 * Time: 11:59
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {



    private static final int COUNT = 4;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                return new ItemsFragment();

            case 1:
                return new SettingFragment();

            default:
                return new BlankFragment();

        }
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {

            case 0:
                return "Home";

            case 1:
                return "Settings";

            case 2:
                return "History";

            case 3:
                return "Store";


            default:
                return "Home";
        }


    }
}
