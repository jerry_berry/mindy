package com.aimindy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aimindy.R;
import com.aimindy.model.History;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.List;

/**
 * Created by android on 1/23/2017.
 */

public class HistoryAdapter extends ArrayAdapter<History> {
    private LayoutInflater mInflater;
    private ImageLoader mLoader;
    private DisplayImageOptions mOptions;

    public HistoryAdapter(Context context, List<History> objects) {
        super(context, 0, objects);
        mInflater = LayoutInflater.from(context);
        mLoader = ImageLoader.getInstance();
        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .showImageOnFail(R.drawable.default_image)
                .showImageOnLoading(R.drawable.default_image)
                .showImageForEmptyUri(R.drawable.default_image)
                .build();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_history_items, parent, false);
            holder = new HistoryAdapter.ViewHolder(convertView);
        } else {
            holder = (HistoryAdapter.ViewHolder) convertView.getTag();
        }
        holder.build(position);
        return convertView;
    }

    private class ViewHolder {
        private ImageView image;
        private TextView item_name;
        private TextView description;
        private TextView points;
        private String prevUrl;


        public ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            item_name = (TextView) view.findViewById(R.id.item_name);
            description = (TextView) view.findViewById(R.id.txt_desc);
            points = (TextView) view.findViewById(R.id.txt_points);
            view.setTag(this);
        }

        public void build(int position) {
            History item = getItem(position);
            if (item.itemName != null)
                item_name.setText(item.itemName);
            else
                item_name.setText("");
            if (item.itemDescription != null)
                description.setText(item.itemDescription);
            else
                description.setText("");


            if (item.itemPicture != null) {

                if (!item.itemPicture.equals(prevUrl)) {
                    mLoader.displayImage(item.itemPicture, image, mOptions);
                    prevUrl = item.itemPicture;
                }
            } else {
                image.setImageResource(R.drawable.default_image);
            }
        }
    }
}
