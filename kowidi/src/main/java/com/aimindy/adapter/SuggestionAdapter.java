package com.aimindy.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aimindy.model.Restoran;

/**
 * Date: 01.12.16
 * Time: 18:11
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class SuggestionAdapter extends ArrayAdapter<Restoran> {


//    private static final int LAYOUT = R.layout.item_suggestion;
    private static final int LAYOUT = android.R.layout.simple_list_item_2;


    public SuggestionAdapter(Context context) {
        super(context, LAYOUT);
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        Restoran restoran = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(LAYOUT, null);

        }

        ((TextView) convertView.findViewById(android.R.id.text1))
                .setText(restoran.getRestaurantName());
        ((TextView) convertView.findViewById(android.R.id.text2))
                .setText(restoran.getRestaurantAddress());

        return convertView;

    }
}
