package com.aimindy.adapter;

/**
 * Created by android on 1/31/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aimindy.R;

public class AllergiesAdapter extends BaseAdapter {
    private Context mContext;
    private final String[] id;
    private final int[] Imageid;
    Integer[] values_gray = new Integer[]{R.drawable.a_gray,R.drawable.b_gray,R.drawable.c_gray,R.drawable.d_gray
            ,R.drawable.e_gray,R.drawable.f_gray,R.drawable.g_gray,R.drawable.h_gray,
            R.drawable.l_gray,R.drawable.m_gray,R.drawable.n_gray
            ,R.drawable.o_gray,R.drawable.p_gray,R.drawable.r_gray

    };

    String [] id_gray = new String[]{"aa","bb","cc","dd","ee","ff","gg","hh","ll","mm","nn","oo","pp","rr"};

    public AllergiesAdapter(Context c, String[] id, int[] Imageid) {
        mContext = c;
        this.id = id;
        this.Imageid = Imageid;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Imageid.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.welcome_slide4_items, null);
            TextView txt_id= (TextView) grid.findViewById(R.id.txt_id);
            ImageView imageView = (ImageView) grid.findViewById(R.id.img_allergies);
            imageView.setImageResource(Imageid[position]);
            txt_id.setText(id[position]);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(txt_id.getText().toString().equals(id_gray[position]))
                    {
                        imageView.setImageResource(Imageid[position]);
                        txt_id.setText(id[position]);

                    }
                    else {
                        txt_id.setText(id_gray[position]);
                        imageView.setImageResource(values_gray[position]);
                    }
                }
            });

        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}