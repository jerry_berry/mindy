package com.aimindy.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;

import android.widget.TextView;

import timber.log.Timber;

/**
 * Date: 01.12.16
 * Time: 19:04
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class SuggestionCursorAdapter extends SimpleCursorAdapter {

    public SuggestionCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Timber.d("bint item: %s",  cursor.getString(0));

        TextView nameView = (TextView) view.findViewById(android.R.id.text1);
        TextView addrView = (TextView) view.findViewById(android.R.id.text2);

        if (cursor != null) {
            nameView.setText(cursor.getString(cursor.getColumnIndex("name")));
            addrView.setText(cursor.getString(cursor.getColumnIndex("address")));
        } else {
            nameView.setText("");
            addrView.setText("");
        }





    }
}
