package com.aimindy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.aimindy.R;
import com.aimindy.model.Item;

import java.util.List;

/**Endless adapter wrapepr for items
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 11.02.2016.
 */
public class EndlessItemsAdapter extends EndlessAdapter {
    private LayoutInflater mInflater;
    private ItemsAdapter mAdapter;
    private NewDataRequest mRequest;
    public interface NewDataRequest{
        /**
         * Method called when adapter needs new data
         * @return is new data exists
         */
        boolean needNewData();
    }

    /**
     * Create new adapter
     * @param context - the context
     * @param items - list of items
     * @param keepOnAppending - false if new data will not available, true if will be
     * @param request - listener for new data requirement event
     */
    public EndlessItemsAdapter(Context context,List<Item> items,boolean keepOnAppending,NewDataRequest request){
        super(new ItemsAdapter(context,items),keepOnAppending);
        mAdapter = (ItemsAdapter)getWrappedAdapter();
        mInflater = LayoutInflater.from(context);
        mRequest = request;
    }

    @Override
    protected boolean cacheInBackground() throws Exception {
        return mRequest!=null&&mRequest.needNewData();
    }

    @Override
    protected void appendCachedData() {

    }

    @Override
    protected View getPendingView(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.layout_item, parent,false);

        ViewGroup data = (ViewGroup)view.findViewById(R.id.layout_main);

        data.setVisibility(View.GONE);

        View progress=view.findViewById(R.id.layout_progress);
        progress.setVisibility(View.VISIBLE);
        return view;
    }

    /**
     * Call this method on data ready to add
     * @param newItems new items to add
     */
    public void onDataReady(List<Item> newItems) {
        mAdapter.addAll(newItems);
        onDataReady();
    }

}
