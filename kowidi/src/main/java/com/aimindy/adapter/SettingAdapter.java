package com.aimindy.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aimindy.R;
import com.aimindy.model.SettingData;

import java.util.ArrayList;

/**
 * Created by android on 1/5/2017.
 */

public class SettingAdapter extends ArrayAdapter<SettingData> {

    private ArrayList<SettingData> dataSet;
    Context mContext;
    public static ArrayList<String> arrayList;


    // View lookup cache
    private static class ViewHolder {
        TextView allrgy_id,allrgy_name;
        ImageView allrgy_image;
        boolean clickable = false;
    }

    public SettingAdapter(ArrayList<SettingData> data, Context context) {
        super(context, R.layout.fragment_setting_items, data);
        this.dataSet = data;
        this.mContext=context;
        arrayList = new ArrayList<>();

    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        SettingData dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag


        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.fragment_setting_items, parent, false);
            viewHolder.allrgy_id = (TextView) convertView.findViewById(R.id.allrgy_id);
            viewHolder.allrgy_name = (TextView) convertView.findViewById(R.id.allrgy_name);
            viewHolder.allrgy_image = (ImageView) convertView.findViewById(R.id.allrgy_image);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.allrgy_id.setText(dataModel.getAllergy_No());
        viewHolder.allrgy_name.setText(dataModel.getAllergy_name());
        viewHolder.allrgy_image.setImageResource(dataModel.getAllergy_image());

        viewHolder.allrgy_id.setTag(position);
        viewHolder.allrgy_id.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!viewHolder.clickable) {
                    viewHolder.allrgy_id.setBackgroundColor(getContext().getResources().getColor(R.color.allergy_selected));
                    viewHolder.clickable = true;
                    arrayList.add(viewHolder.allrgy_id.getText().toString().trim());
                }
                else
                {
                    viewHolder.allrgy_id.setBackgroundColor(getContext().getResources().getColor(R.color.allergy_disable));
                    viewHolder.clickable = false;
                    arrayList.remove(viewHolder.allrgy_id.getText().toString().trim());

                }

            }

        });
        // Return the completed view to render on screen
        return convertView;
    }
}
