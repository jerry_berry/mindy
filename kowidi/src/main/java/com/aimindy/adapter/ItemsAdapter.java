package com.aimindy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aimindy.R;
import com.aimindy.model.Item;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.List;

/** List items adapter
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 11.02.2016.
 */
public class ItemsAdapter extends ArrayAdapter<Item> {
    private LayoutInflater mInflater;
    private ImageLoader mLoader;
    private DisplayImageOptions mOptions;

    public ItemsAdapter(Context context, List<Item> objects) {
        super(context, 0, objects);
        mInflater = LayoutInflater.from(context);
        mLoader = ImageLoader.getInstance();
        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .showImageOnFail(R.drawable.default_image)
                .showImageOnLoading(R.drawable.default_image)
                .showImageForEmptyUri(R.drawable.default_image)
                .build();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView==null){
            convertView = mInflater.inflate(R.layout.layout_item,parent,false);
            holder = new ViewHolder(convertView);
        }
        else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.build(position);
        return convertView;
    }

    private class ViewHolder{
        private ImageView image;
        private TextView title;
        private TextView description;
        private TextView price;
        private TextView distance;
        private TextView units;
        private TextView txt_resturentName;
        private String prevUrl;
        public ViewHolder(View view){
            image = (ImageView)view.findViewById(R.id.image);
            title = (TextView)view.findViewById(R.id.title);
            description = (TextView)view.findViewById(R.id.description);
            price = (TextView)view.findViewById(R.id.price);
            distance = (TextView)view.findViewById(R.id.distance);
            units = (TextView)view.findViewById(R.id.units);
            txt_resturentName =(TextView) view.findViewById(R.id.txt_resturentName);
            view.setTag(this);
        }
        public void build(int position){
            Item item = getItem(position);
            if (item.itemName!=null)
                title.setText(item.itemName);
            else
                title.setText("");
            if (item.itemDescription!=null)
                description.setText(item.itemDescription);
            else
                description.setText("");
            if (item.itemPrice!=null)
                price.setText(getContext().getString(R.string.price_format,item.itemPrice));
            else
                price.setText("");


            if (item.restaurantName!=null)
                txt_resturentName.setText(item.restaurantName);
            else
                txt_resturentName.setText("");
            if (item.itemDistance!=null){
                // Format distance, <500 m - to meters, >500 meters to km.
                String value = item.itemDistance;
                value = value.replace(",","");
                double meters = 0;
                try {
                    meters = Double.parseDouble(value);
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }
                if (meters==0){
                    distance.setText(R.string.unknown);
                    units.setText("");
                }
                else if (meters<=500) {
                    distance.setText(String.format("%.0f",meters));
                    units.setText(R.string.units_meters);
                }
                else{
                    distance.setText(String.format("%.1f",meters/1000f));
                    units.setText(R.string.units_kilometers);
                }
            }
            if (item.itemPicture!=null){

                if (!item.itemPicture.equals(prevUrl)) {
                    mLoader.displayImage(item.itemPicture, image,mOptions);
                    prevUrl = item.itemPicture;
                }
            }
            else{
                image.setImageResource(R.drawable.default_image);
            }
        }
    }
}
