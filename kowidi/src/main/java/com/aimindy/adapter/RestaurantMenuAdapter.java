package com.aimindy.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aimindy.KowidiApp;
import com.aimindy.R;
import com.aimindy.model.Item;
import com.aimindy.mvp.common.RxBus;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.List;

import javax.inject.Inject;

/**
 * Date: 02.12.16
 * Time: 06:50
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class RestaurantMenuAdapter extends ArrayAdapter<Item> {



    private static final int LAYOUT = R.layout.item_menu;



    private ImageLoader mLoader;
    private DisplayImageOptions mOptions;


    @Inject
    RxBus _rxBus;


    public RestaurantMenuAdapter(Context context, List<Item> objects) {
        super(context, 0, objects);

        // resolve dependencies
        KowidiApp.getAppComponent().inject(this);

        mLoader = ImageLoader.getInstance();
        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .showImageOnFail(R.drawable.default_image)
                .showImageOnLoading(R.drawable.default_image)
                .showImageForEmptyUri(R.drawable.default_image)
                .build();
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RestaurantMenuAdapter.ViewHolder holder;

        if (convertView==null){

            convertView = LayoutInflater.from(getContext())
                    .inflate(LAYOUT, parent,false);

            holder = new RestaurantMenuAdapter.ViewHolder(convertView, getContext().getString(R.string.price_format));
        }
        else {
            holder = (RestaurantMenuAdapter.ViewHolder)convertView.getTag();
        }


        Item item = getItem(position);


        holder.uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Timber.d("click upload on %s", item.itemId);
                _rxBus.send(new RxBus.UploadPhotoEvent(item.itemId));
            }
        });



        holder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Timber.d("click root layout on %s", item.itemId);
                _rxBus.send(new RxBus.MenuTapEvent(item.itemId, item.itemCodeName));
            }
        });



        holder.bind(item);
        return convertView;

    }





    public  class ViewHolder {

        private ImageView image;
        private TextView title;
        private TextView description;
        private TextView price;
        private String prevUrl;
        private String resPrice;
        private Button uploadButton;
        private View rootLayout;

        public ViewHolder(View view, String resPrice) {
            image = (ImageView)view.findViewById(R.id.image);
            title = (TextView)view.findViewById(R.id.title);
            description = (TextView)view.findViewById(R.id.description);
            price = (TextView)view.findViewById(R.id.price);
            uploadButton = (Button) view.findViewById(R.id.upload_button);
            rootLayout = view.findViewById(R.id.content);
            this.resPrice = resPrice;
            view.setTag(this);
        }


        public void bind(Item item) {

            if (item.itemName!=null)
                title.setText(item.itemName);
            else
                title.setText("");
            if (item.itemDescription!=null)
                description.setText(item.itemDescription);
            else
                description.setText("");
            if (item.itemPrice!=null)
                price.setText(String.format(resPrice, item.itemPrice));
            else
                price.setText("");


            if (item.itemPicture!=null){

                if (!item.itemPicture.equals(prevUrl)) {
                    mLoader.displayImage(item.itemPicture, image,mOptions);
                    prevUrl = item.itemPicture;
                }
            }
            else{
                image.setImageResource(R.drawable.default_image);
            }

        }

    }
}
