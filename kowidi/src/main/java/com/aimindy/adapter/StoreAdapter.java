package com.aimindy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aimindy.R;
import com.aimindy.model.StoreData;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.List;

/**
 * Created by android on 1/24/2017.
 */

public class StoreAdapter extends  ArrayAdapter<StoreData>
{
    private LayoutInflater mInflater;
    private ImageLoader mLoader;
    private DisplayImageOptions mOptions;

    public StoreAdapter(Context context, List<StoreData> objects) {
        super(context, 0, objects);
        mInflater = LayoutInflater.from(context);
        mLoader = ImageLoader.getInstance();
        mOptions = new DisplayImageOptions.Builder()
        .cacheInMemory(true)
        .cacheOnDisk(true)
        .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
        .showImageOnFail(R.drawable.default_image)
        .showImageOnLoading(R.drawable.default_image)
        .showImageForEmptyUri(R.drawable.default_image)
        .build();

        }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StoreAdapter.ViewHolder holder;
        if (convertView == null) {
        convertView = mInflater.inflate(R.layout.fragment_store_items, parent, false);
        holder = new StoreAdapter.ViewHolder(convertView);
        } else {
        holder = (StoreAdapter.ViewHolder) convertView.getTag();
        }
        holder.build(position);
        return convertView;
        }

private class ViewHolder {
    private ImageView img_activity;
    private TextView txt_name;



    public ViewHolder(View view) {
        img_activity = (ImageView) view.findViewById(R.id.img_activity);
        txt_name = (TextView) view.findViewById(R.id.txt_name);

        view.setTag(this);
    }

    public void build(int position) {
        StoreData item = getItem(position);
        if (item.getName() != null)
            txt_name.setText(item.getName());
        else
            txt_name.setText("");
        }
    }
}
