package com.aimindy.network;

import com.aimindy.entity.User;

/**
 * Date: 28.11.16
 * Time: 12:17
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class LoginResponse {

    private String cookie;
    private String session;
    private String status;
    private String accessToken;
    private User user;


    // if error
    private String error;
    private String message;

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "cookie='" + cookie + '\'' +
                ", session='" + session + '\'' +
                ", status='" + status + '\'' +
                ", user=" + user +
                ", error='" + error + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
