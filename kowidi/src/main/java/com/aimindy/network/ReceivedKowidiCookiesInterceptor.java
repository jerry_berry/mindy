package com.aimindy.network;

import com.aimindy.mvp.common.SessionDataStore;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Date: 05.12.16
 * Time: 16:56
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class ReceivedKowidiCookiesInterceptor implements Interceptor {


    SessionDataStore sessionDataStore;


    public ReceivedKowidiCookiesInterceptor(SessionDataStore sessionDataStore) {
        this.sessionDataStore = sessionDataStore;
    }



    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {


            // we no need old cookies
            HashSet<String> cookies = new HashSet<>();


            for (String header : originalResponse.headers("Set-Cookie")) {
                cookies.add(header);
                Timber.d("found fresh cookie from server respose: %s", header);
            }


            sessionDataStore.setRealCookies(cookies);
        }

        return originalResponse;
    }
}
