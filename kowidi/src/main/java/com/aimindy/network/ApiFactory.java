package com.aimindy.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.aimindy.AppConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Init retrofit interface and provide api services
 */
public class ApiFactory {
    private static final int CONNECT_TIMEOUT = 15;
    private static final int WRITE_TIMEOUT = 60;
    private static final int TIMEOUT = 60;

    private static final OkHttpClient CLIENT;
    private static final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new SystemLogger());
    private static final String TAG = "ApiFactory";


    static {
        loggingInterceptor.setLevel(AppConstants.API_DEBUG_LEVEL);
        CLIENT = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor).build();
    }

    @NonNull
    public static ApiService getApiService() {
        return getRetrofit().create(ApiService.class);
    }

    @NonNull
    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(AppConstants.API_LINK)
                .client(CLIENT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    private static class SystemLogger implements HttpLoggingInterceptor.Logger{

        @Override
        public void log(String message) {
            Log.d(TAG,message);
        }
    }
}
