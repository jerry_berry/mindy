package com.aimindy.network;


import com.aimindy.model.HistoryWrapper;
import com.aimindy.model.ItemResponse;
import com.aimindy.model.ItemWrapper;
import com.aimindy.model.RegisterResponse;
import com.aimindy.model.RestaurantResponse;
import com.aimindy.model.SettingResponse;
import com.aimindy.model.StoreWrapper;
import com.aimindy.model.SuggestionResponse;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {


    @GET("/Home?format=json")
    Call<ItemWrapper> getItems(@Header("access_token") String access_token,@Query("lat")double lat, @Query("lng")double lng, @Query("start")int offset);

    @GET("/Home?format=json")
    Call<ItemWrapper> search(@Header("access_token") String access_token,@Query("q")String query,@Query("lat")double lat,@Query("lng")double lng,@Query("start")int offset,@Query("speak")boolean speak);

    @GET("/ViewItem?format=json")
    Call<ItemResponse> getItem(@Header("access_token") String access_token, @Query("itemid")long itemId,@Query("codename")String itemCodeName);


    @POST("/Login?format=json")
    Observable<LoginResponse> login(@Query("userAuth") String email, @Query("password") String password);


    @GET("/FacebookLoginApp?format=json")
    Observable<LoginResponse> facebookLogin(@Query("facebookid") String key);


    @GET("/RestaurantAutoSuggest?format=json")
    Observable<SuggestionResponse> suggest(@Query("q") String query);


    @GET("/viewrestaurant?format=json")
    Observable<RestaurantResponse> getRestaurant(@Header("access_token") String access_token, @Query("restaurantid") String restaurantid);

    @Multipart
    @POST("/Picture?format=json")
    Observable<UploadPhotoResponse> uploadPhoto(@Header("access_token") String access_token,
                                                @Query("itemid")long itemId,
                                                @Part MultipartBody.Part file);
    @POST("/Setting?format=json")
    Call<SettingResponse> saveSetting(@Header("access_token") String access_token,@Query("language") String language, @Query("allergies") String allergies, @Query("dietcategory") String dietcategory, @Query("user_id") String user_id,@Query("username") String username );

    @POST("/Register?format=json")
    Call<RegisterResponse> register(@Query("useremail") String useremail, @Query("password") String password, @Query("retypedpassword") String retypedpassword);

    @GET("/History?format=json")
    Call<HistoryWrapper> getHistory(@Header("access_token") String access_token);

    @GET("/Profile?format=json")
    Call<StoreWrapper> getStore(@Header("access_token") String access_token);
}
