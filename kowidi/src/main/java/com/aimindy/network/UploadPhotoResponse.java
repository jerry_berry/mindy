package com.aimindy.network;

/**
 * Date: 05.12.16
 * Time: 15:26
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class UploadPhotoResponse {

    private String status;
    private String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
