package com.aimindy.network;

import com.aimindy.mvp.common.SessionDataStore;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Date: 05.12.16
 * Time: 16:41
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class AddKowidiCookiesInterceptor implements Interceptor {

    SessionDataStore sessionDataStore;


    public AddKowidiCookiesInterceptor(SessionDataStore sessionDataStore) {
        this.sessionDataStore = sessionDataStore;
        Timber.d("I'm here and ready");
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request.Builder builder = chain.request().newBuilder();


        HashSet<String> cookies = (HashSet<String>) sessionDataStore.getRealCookes();
        for (String cookie : cookies) {
            builder.addHeader("Cookie", cookie);
            Timber.d("add cookie: %s", cookie);
        }



        return chain.proceed(builder.build());

    }
}
