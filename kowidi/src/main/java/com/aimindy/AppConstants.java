package com.aimindy;


import com.aimindy.network.HttpLoggingInterceptor;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 11.02.2016.
 */
public class AppConstants {
    /**
     * Web requests log level
     */
    public static final HttpLoggingInterceptor.Level API_DEBUG_LEVEL = HttpLoggingInterceptor.Level.BODY;

    /**
     * Base api url
     */
//    public static final String API_LINK = "http://kowidi.com";
    public static final String API_LINK = "https://aimindy.com";
    //public static final String API_LINK = "http://10.0.0.1:8084/aimindy";
    /**
     * Max height of the snackbar
     */
    public static final int SNACKBAR_MAX_HEIGHT = 6;

    /**
     * Default page size
     */
    public static final int PAGE_SIZE = 30;
    /**
     * Max time difference to gps be valid
     */
    public static final long MAX_GOOD_LOCATION_DIFFERENCE = 2 * 60 * 1000;
}
