package com.aimindy;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.facebook.AccessToken;
import com.aimindy.helper.PrefManager;
import com.aimindy.mvp.common.SessionDataStore;
import com.aimindy.mvp.common.TokenDataStore;
import com.aimindy.mvp.common.UserDataStore;
import com.aimindy.ui.BackButtonHandler;
import com.aimindy.ui.CustomFragment;
import com.aimindy.ui.DetailsFragment;
import com.aimindy.ui.HistoryFragment;
import com.aimindy.ui.ItemsFragment;
import com.aimindy.ui.OnFragmentInteractionListener;
import com.aimindy.ui.activity.DetailActivity;
import com.aimindy.ui.activity.LoginActivity;
import com.aimindy.ui.activity.RestaurantActivity;
import com.aimindy.ui.fragment.RestaurantFragment;
import com.aimindy.ui.fragment.SettingFragment;
import com.aimindy.ui.fragment.StoreFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    private static final int LAYOUT = R.layout.activity_main;

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    public static final String ACTION_LOCATION_UPDATE = MainActivity.class.getName() + ".LOCATION_UPDATE";
    private static final String TAG = "MainActivity";

    // tags used to attach the fragments
    private static final String TAG_HOME = "ItemsFragment";
    private static final String TAG_HISTORY = "HistoryFragment";
    private static final String TAG_SETTING = "SettingFragment";
    private static final String TAG_STORE = "StoreFragment";
    private static final String TAG_LOGOUT = "Logout";

    public static String CURRENT_TAG = TAG_HOME;
    private FragmentManager mFm;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private PrefManager prefManager;
    Toolbar myToolbar;
    // index to identify current nav menu item
    public static int navItemIndex = 0;
    private Location mLocation = null;
    private LocationManager mLm;

    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    private String[] activityTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);

        if (checkLocationPermissions()) requestCoordinates();


        prefManager = new PrefManager(this);
        mHandler = new Handler();

        myToolbar= (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);

        navHeader = navigationView.getHeaderView(0);
        mFm = getSupportFragmentManager();
        mFm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                ActionBar ab = getSupportActionBar();
                boolean mShowBack = mFm.getBackStackEntryCount() > 1;
                if (ab != null) {
                    ab.setDisplayHomeAsUpEnabled(mShowBack);
                }
            }
        });

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);//Menu Resource, Menu
        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==android.R.id.home){
            onBackPressed();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

    private boolean checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)){
                showLocationPermissionDescription();
            }
            else
                requestLocationPermission();
            return false;
        }
        return true;
    }
    private void showLocationPermissionDescription() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_location_permission);
        builder.setMessage(R.string.msg_location_permission);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestLocationPermission();
            }
        });
        builder.show();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void requestLocationPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissions.size()>0){
            String[] perms = new String[permissions.size()];
            perms = permissions.toArray(perms);
            ActivityCompat.requestPermissions(this, perms, REQUEST_LOCATION_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode==REQUEST_LOCATION_PERMISSION&& grantResults[0] == PackageManager.PERMISSION_GRANTED){
            requestCoordinates();
        }
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressWarnings("ResourceType")
    private void requestCoordinates() {
        Log.d(TAG,"Request coordinates" );
        mLm = (LocationManager)getSystemService(LOCATION_SERVICE);
        if (mLm!=null){
            Location lGps = null;
            Location lNetwork = null;
            if (mLm.isProviderEnabled(LocationManager.GPS_PROVIDER))
                lGps = mLm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (mLm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
                lNetwork = mLm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (lGps==null){
                mLocation = lNetwork;
            }
            else{
                if (lNetwork==null)
                    mLocation = lGps;
                else if (lGps.getTime()<lNetwork.getTime()-AppConstants.MAX_GOOD_LOCATION_DIFFERENCE)
                    mLocation = lNetwork;
                else
                    mLocation = lGps;
            }
            if (mLocation!=null){
                notifyLocation();
            }
            if (mLm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                mLm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListenerGps);
            }
            if (mLm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                mLm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,mLocationListenerNetwork);
            }
        }
    }

    private LocationListener mLocationListenerGps = new LocationListener() {
        @SuppressWarnings("ResourceType")
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG,"location changed gps" );
            Location prevLocation = mLocation;
            mLocation = location;
            if (mLocationListenerGps != null){
                mLm.removeUpdates(mLocationListenerGps);
                mLocationListenerGps = null;
            }
            if (prevLocation==null)
                notifyLocation();

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
    private LocationListener mLocationListenerNetwork = new LocationListener() {
        @SuppressWarnings("ResourceType")
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG,"location changed network" );
            Location prevLocation = mLocation;
            if (mLocation==null||!LocationManager.GPS_PROVIDER.equals(mLocation.getProvider())||mLocation.getTime()<location.getTime()-AppConstants.MAX_GOOD_LOCATION_DIFFERENCE)
                mLocation = location;
            if (mLocationListenerNetwork != null){
                mLm.removeUpdates(mLocationListenerNetwork);
                mLocationListenerNetwork = null;
            }
            if (prevLocation==null)
                notifyLocation();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }

    };
    private void notifyLocation() {
        Log.d(TAG,"Notify location" );
        Intent intent = new Intent(ACTION_LOCATION_UPDATE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /*private void toMainFragment() {
        ItemsFragment f = new ItemsFragment();
        switchFragment(f,f.getFragmentTag(),true,false,true);
    }
*/
    @Override
    public void onBackPressed() {
        boolean isBackButtonAllowed = true;
        if (mFm.getBackStackEntryCount()>0) {
            String tag = mFm.getBackStackEntryAt(mFm.getBackStackEntryCount() - 1).getName();
            Fragment f = mFm.findFragmentByTag(tag);
            if (f!=null&&f instanceof BackButtonHandler){
                isBackButtonAllowed = ((BackButtonHandler)f).onBackButtonPressed();
            }
        }
        if (isBackButtonAllowed) {
            if (mFm.getBackStackEntryCount() > 1)
                mFm.popBackStack();
            else
                finish();
        }

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();

    }

    private void switchFragment(CustomFragment fragment, String tag,boolean addToBackStack, boolean clearStack,boolean mainFragment) {

        if (clearStack){
            mFm.popBackStackImmediate(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        FragmentTransaction ft = mFm.beginTransaction();
        ft.setCustomAnimations(mainFragment?0:R.anim.enter, R.anim.exit, R.anim.pop_enter, mainFragment?R.anim.exit:R.anim.pop_exit);
        ft.replace(R.id.container, fragment, tag);
        if (addToBackStack)
            ft.addToBackStack(tag);
        ft.commit();
    }

    private Fragment revertFragment(String tag) {
        Fragment fr = mFm.findFragmentByTag(tag);
        mFm.popBackStackImmediate(tag, 0);
        return fr;
    }

    @Override
    public View getSnackAnchor() {
        return null;
    }

    @Override
    public double getLatitude() {
        return mLocation!=null?mLocation.getLatitude():0d;
    }

    @Override
    public double getLongitude() {
        return mLocation!=null?mLocation.getLongitude():0d;
    }


    @SuppressWarnings("ResourceType")
    @Override
    protected void onDestroy() {
        if (mLm!=null) {
            if (mLocationListenerNetwork != null)
                mLm.removeUpdates(mLocationListenerNetwork);
            if (mLocationListenerGps != null)
                mLm.removeUpdates(mLocationListenerGps);
        }
        super.onDestroy();
    }
    @Override
    public void back(String parentTag){
        revertFragment(parentTag);
    }

    @Override
    public void showItemDetails(CustomFragment fragment, long itemId, String itemCodeName) {
//        DetailsFragment f = DetailsFragment.newInstance(fragment.getFragmentTag(),itemId,itemCodeName);
//        switchFragment(f,f.getFragmentTag(),true,false,false);


        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailsFragment.ARG_ITEM_ID, itemId);
        intent.putExtra(DetailsFragment.ARG_CODENAME, itemCodeName);

        startActivity(intent);
    }

    @Override
    public void showRestaurant(CustomFragment fragment, String itemId, String itemCodeName) {

//        RestaurantFragment f = RestaurantFragment.newInstance(fragment.getFragmentTag(), itemId, itemCodeName);
//        switchFragment(f,f.getFragmentTag(),true,false,false);


        Intent intent = new Intent(this, RestaurantActivity.class);
        intent.putExtra(RestaurantFragment.EXTRA_RESTARAUNT_ID, itemId);
        intent.putExtra(RestaurantFragment.EXTRA_CODE_NAME, itemCodeName);

        startActivity(intent);
    }

    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }


        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                ItemsFragment homeFragment = new ItemsFragment();
                return homeFragment;
            case 1:
                // History
                HistoryFragment historyFragment = new HistoryFragment();
                return historyFragment;
            case 2:
                // store
                StoreFragment storeFragment = new StoreFragment();
                return storeFragment;
            case 3:
                // settings
                SettingFragment settingFragment = new SettingFragment();
                return settingFragment;


            default:
                return new ItemsFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.nav_history:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_HISTORY;
                        break;
                    case R.id.nav_store:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_STORE;
                        break;
                    case R.id.nav_setting:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_SETTING;
                        break;
                    case R.id.nav_logout:
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Logout")
                                .setMessage("Would you like to logout?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        TokenDataStore.clearData();
                                        SessionDataStore.clearData();
                                        UserDataStore.clearUser();
                                        AccessToken.setCurrentAccessToken(null);

                                        prefManager.setFirstTimeLaunch(true);
                                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                                        startMain.addCategory(Intent.CATEGORY_HOME);
                                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                        startActivity(startMain);
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);

                                        finish();
                                        System.exit(0);

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                        break;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, myToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


}
