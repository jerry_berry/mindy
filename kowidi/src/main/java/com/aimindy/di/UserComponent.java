package com.aimindy.di;

import com.aimindy.di.module.UserModule;
import com.aimindy.di.scope.UserScope;

import dagger.Subcomponent;

/**
 * Date: 28.11.16
 * Time: 16:32
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@UserScope
@Subcomponent(modules = UserModule.class)
public interface UserComponent {


    @Subcomponent.Builder
    interface Builder {
        Builder sessionrModule(UserModule userModule);
        UserComponent build();
    }





}
