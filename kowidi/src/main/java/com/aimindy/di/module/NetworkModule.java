package com.aimindy.di.module;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.aimindy.AppConstants;
import com.aimindy.mvp.common.SessionDataStore;
import com.aimindy.network.AddCookiesInterceptor;
import com.aimindy.network.AddKowidiCookiesInterceptor;
import com.aimindy.network.ApiService;
import com.aimindy.network.ReceivedCookiesInterceptor;
import com.aimindy.network.ReceivedKowidiCookiesInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Date: 28.11.16
 * Time: 11:47
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

@Module
public class NetworkModule {


    @Singleton
    @Provides
    RxJava2CallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io());
    }


    @Singleton
    @Provides
    AddCookiesInterceptor provideAddCookieInterceptro(Context context) {
        return new AddCookiesInterceptor(context);
    }


    @Singleton
    @Provides
    ReceivedCookiesInterceptor provideReceivedCookieInterceptor(Context context) {
        return new ReceivedCookiesInterceptor(context);
    }



    @Singleton
    @Provides
    AddKowidiCookiesInterceptor provideAddKowidiIntercaptor(SessionDataStore sessionDataStore) {
        return new AddKowidiCookiesInterceptor(sessionDataStore);
    }


    @Singleton
    @Provides
    ReceivedKowidiCookiesInterceptor provideReceivedKowidiCookiesInterceptro(SessionDataStore sessionDataStore) {
        return new ReceivedKowidiCookiesInterceptor(sessionDataStore);
    }


    @Singleton
    @Provides
    OkHttpClient provideHttpClient(AddKowidiCookiesInterceptor kowidiCookiesInterceptor,
                                   ReceivedKowidiCookiesInterceptor receivedKowidiCookiesInterceptor){
        return new OkHttpClient
                .Builder()
//                .addInterceptor(addCookiesInterceptor)
//                .addInterceptor(receivedCookiesInterceptor)
                .addInterceptor(kowidiCookiesInterceptor)
                .addInterceptor(receivedKowidiCookiesInterceptor)
                .build();

    }


    @Singleton
    @Provides
    Retrofit provideRetrofit(RxJava2CallAdapterFactory rxJavaCallAdapterFactory,
                             OkHttpClient client) {


        return new Retrofit.Builder()
                .baseUrl(AppConstants.API_LINK)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .client(client)
                .build();

    }


    @Singleton
    @Provides
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }



}
