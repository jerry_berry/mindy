package com.aimindy.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.aimindy.di.UserComponent;
import com.aimindy.mvp.common.RxBus;
import com.aimindy.mvp.common.SessionDataStore;
import com.aimindy.mvp.common.TokenDataStore;
import com.aimindy.mvp.common.UserDataStore;
import com.aimindy.mvp.model.UserManager;
import com.aimindy.network.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Date: 28.11.16
 * Time: 11:40
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@Module
public class AppModule {


    private Application application;


    public AppModule(Application application) {
        this.application = application;
    }


    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }



    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }


    @Provides
    Gson provideGson() {
        return new Gson();
    }


    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }


    @Provides
    @Singleton
    UserDataStore provideUserDataStore(SharedPreferences sharedPreferences, Gson gson) {
        return new UserDataStore(sharedPreferences, gson);
    }



    @Provides
    @Singleton
    TokenDataStore provideTokenDataStore(SharedPreferences sharedPreferences) {
        return  new TokenDataStore(sharedPreferences);
    }


    @Provides
    @Singleton
    SessionDataStore provideSessionDataStore(SharedPreferences sharedPreferences) {
        return new SessionDataStore(sharedPreferences);
    }


    @Provides
    @Singleton
    public UserManager proivdeUserManager(ApiService apiService, TokenDataStore tokenDataStore,
                                          SessionDataStore sessionDataStore,
                                          UserDataStore userDataStore,
                                          UserComponent.Builder userComponentBuilder) {

        return new UserManager(apiService, tokenDataStore, sessionDataStore, userDataStore,
                userComponentBuilder);

    }


}
