package com.aimindy.di.module;

import com.aimindy.di.scope.UserScope;
import com.aimindy.entity.User;

import dagger.Module;
import dagger.Provides;

/**
 * Date: 28.11.16
 * Time: 11:41
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

@Module
public class UserModule {

    private User user;

    public UserModule(User user) {
        this.user = user;
    }

    @UserScope
    @Provides
    User provideUser() {
        return user;
    }



}
