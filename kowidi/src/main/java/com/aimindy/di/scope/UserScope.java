package com.aimindy.di.scope;

import javax.inject.Scope;

/**
 * Date: 28.11.16
 * Time: 16:33
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@Scope
public @interface UserScope {
}
