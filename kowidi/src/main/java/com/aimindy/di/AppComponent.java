package com.aimindy.di;

import com.aimindy.adapter.RestaurantMenuAdapter;
import com.aimindy.di.module.AppModule;
import com.aimindy.di.module.NetworkModule;
import com.aimindy.mvp.presenter.DetailsPresenter;
import com.aimindy.mvp.presenter.LoginPresenter;
import com.aimindy.mvp.presenter.RestaurantPresenter;
import com.aimindy.mvp.presenter.SplashPresenter;
import com.aimindy.mvp.presenter.SuggestionPresenter;
import com.aimindy.mvp.presenter.UploadPhotoPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Date: 28.11.16
 * Time: 11:40
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {



    UserComponent.Builder userComponentBuilder();



    SplashPresenter inject(SplashPresenter presenter);
    LoginPresenter inject(LoginPresenter presenter);

    SuggestionPresenter inject(SuggestionPresenter presenter);
    RestaurantPresenter inject(RestaurantPresenter presenter);

    RestaurantMenuAdapter inject(RestaurantMenuAdapter restaurantMenuAdapter);
    UploadPhotoPresenter inject(UploadPhotoPresenter uploadPhotoPresenter);

    DetailsPresenter inject(DetailsPresenter detailsPresenter);

}
