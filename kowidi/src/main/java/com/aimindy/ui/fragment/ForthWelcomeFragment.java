package com.aimindy.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.aimindy.MainActivity;
import com.aimindy.R;
import com.aimindy.helper.PrefManager;

/**
 * Created by android on 1/23/2017.
 */

public class ForthWelcomeFragment extends Fragment {

    ListView list_category;
    String[] values = new String[] { "Omnivore",
            "Vegeterian",
            "Vegan",

    };
    private PrefManager prefManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.welcome_slide4, container, false);
        prefManager = new PrefManager(getActivity());

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list_category = (ListView) view.findViewById(R.id.list_category);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.welocme_slide3_items, R.id.txt_name,values);


        // Assign adapter to ListView
        list_category.setAdapter(adapter);

        list_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    prefManager.setFirstTimeLaunch(false);
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
            }
        });

    }
}
