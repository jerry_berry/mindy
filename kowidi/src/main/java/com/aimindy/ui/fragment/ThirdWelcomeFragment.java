package com.aimindy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.aimindy.R;
import com.aimindy.ui.activity.WelcomeActivity;

/**
 * Created by android on 1/23/2017.
 */

public class ThirdWelcomeFragment extends Fragment {

    ListView list_language;
    String[] values = new String[] { "english",
            "italian",
            "german",

    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.welcome_slide3, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list_language = (ListView) view.findViewById(R.id.list_language);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.welocme_slide3_items, R.id.txt_name,values);


        // Assign adapter to ListView
        list_language.setAdapter(adapter);

        list_language.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                WelcomeActivity.viewPager.setCurrentItem(3);
            }
        });

    }
}
