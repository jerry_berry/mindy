package com.aimindy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.aimindy.R;
import com.aimindy.adapter.AllergiesAdapter;


/**
 * Created by android on 1/31/2017.
 */

public class FifthWelcomeFragment extends Fragment {

    GridView grid_allergies;
    int[] values = new int[]{R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d
            ,R.drawable.e,R.drawable.f,R.drawable.g,R.drawable.h,R.drawable.l,R.drawable.m,R.drawable.n
            ,R.drawable.o,R.drawable.p,R.drawable.r

    };
    String [] id = new String[]{"a","b","c","d","e","f","g","h","l","m","n","o","p","r"};

    AllergiesAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.welcome_slide5, container, false);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        grid_allergies = (GridView) view.findViewById(R.id.grid_allergies);

       adapter = new AllergiesAdapter(getActivity(),id,values);

        // Assign adapter to ListView
        grid_allergies.setAdapter(adapter);



    }
}