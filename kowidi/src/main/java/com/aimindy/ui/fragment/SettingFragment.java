package com.aimindy.ui.fragment;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.aimindy.R;
import com.aimindy.adapter.SettingAdapter;
import com.aimindy.helper.ConnectionHelper;
import com.aimindy.helper.NotificationHelper;
import com.aimindy.model.SettingData;
import com.aimindy.model.SettingResponse;
import com.aimindy.mvp.model.UserManager;
import com.aimindy.network.ApiFactory;
import com.aimindy.ui.CustomFragment;
import java.util.ArrayList;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by android on 1/5/2017.
 */

public class SettingFragment extends CustomFragment {

    private static final String TAG = "SettingFragment";
    private static final int LAYOUT = R.layout.fragment_setting;
    Spinner spr_language,spr_diet_category;
    String[] languages = new String[]{"English"};
    String[] diet_category = new String[]{"Omnivore","Vegetarian","Vegan"};
    ArrayAdapter<String> array_language,array_dietCategory;
    ListView setting_list;
    EditText edt_username;
    ArrayList<SettingData> array_setting;
    SettingData settingData;
    SettingAdapter adapter;
    Button btn_save;
    private ProgressBar mProgress;
    CompositeDisposable disposable;
    String user_id;

    public static final String[] titles = new String[] { "fish",
            "mollusc", "crustaceans", "celery" ,"soy","gluten","nuts"
            ,"peanuts","sesam","sulphites","lupines","mustard","eggs"
            ,"milk"};

    public static final String[] id = new String[] {
            "A","B","C","D","E","F","G","H","L","M","N","O","P","R"};

    public static final Integer[] images = { R.drawable.a,
            R.drawable.b, R.drawable.c, R.drawable.d,R.drawable.e,R.drawable.f,
            R.drawable.g,R.drawable.h,R.drawable.l,R.drawable.m,
            R.drawable.n,R.drawable.o,R.drawable.p,R.drawable.r};


    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        array_setting = new ArrayList<>();
        disposable = new CompositeDisposable();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgress = (ProgressBar)view.findViewById(R.id.progress);
        spr_language = (Spinner) view.findViewById(R.id.spr_language);
        spr_diet_category = (Spinner) view.findViewById(R.id.spr_diet_category);
        setting_list = (ListView) view.findViewById(R.id.setting_list);
        edt_username = (EditText) view.findViewById(R.id.edt_username);
        btn_save = (Button) view.findViewById(R.id.btn_save);

        array_language = new ArrayAdapter<String>( getActivity(),R.layout.spinner_item,languages);
        array_dietCategory = new ArrayAdapter<String>( getActivity(),R.layout.spinner_item,diet_category);

        array_language.setDropDownViewResource(R.layout.spinner_item);
        spr_language.setAdapter(array_language);

        array_dietCategory.setDropDownViewResource(R.layout.spinner_item);
        spr_diet_category.setAdapter(array_dietCategory);

        setting_list();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                SaveSetting();

            }
        });
    }

    private void setting_list() {
        for (int i = 0; i < titles.length; i++) {
            settingData = new SettingData(id[i], images[i], titles[i]);
            array_setting.add(settingData);
        }

        adapter = new SettingAdapter(array_setting,getActivity());
        setting_list.setAdapter(adapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                SaveSetting();
                return true;
           
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void SaveSetting() {

        if (!ConnectionHelper.isConnected(getContext())) {
            NotificationHelper.showErrorNoInternet(getSnackAnchor());
            mProgress.setVisibility(View.GONE);
            return;
        }

        mProgress.setVisibility(View.VISIBLE);
        StringBuilder sb = new StringBuilder();
        for(String str : adapter.arrayList){
            sb.append(str);
        }

        Call<SettingResponse> call = ApiFactory.getApiService().saveSetting(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("SESSION", "defaultStringIfNothingFound"),spr_language.getSelectedItem().toString(), sb.toString(), spr_diet_category.getSelectedItem().toString(),UserManager.user.getId(),edt_username.getText().toString().trim());
        call.enqueue(new CustomCallback<SettingResponse>(call) {

                 @Override
                 public void onResponse(Call<SettingResponse> call, Response<SettingResponse> response)
                 {
                     mProgress.setVisibility(View.GONE);
                     Log.d("response-->",response.body()+"");
                     if(response.body().getStatus().toLowerCase().equals("success"))
                     {
                         Toast.makeText(getActivity(), "Setting Save Successfully", Toast.LENGTH_LONG).show();

                     }
                     else
                     {
                         Toast.makeText(getActivity(), "Can't Save Setting", Toast.LENGTH_LONG).show();
                     }
                 }

                 @Override
                 public void onFailure(Call<SettingResponse> call, Throwable t) {
                    mProgress.setVisibility(View.GONE);
                 }
             });
        }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    private abstract class CustomCallback<T> implements Callback<T> {
        protected Call<T> localCall;
        public CustomCallback(Call<T> call){
            localCall = call;
        }
    }


}
