package com.aimindy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aimindy.R;

/**
 * Date: 02.12.16
 * Time: 12:02
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class BlankFragment extends Fragment {



    private static final int LAYOUT = R.layout.fragment_blank;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        return view;
    }
}
