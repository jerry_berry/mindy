package com.aimindy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.aimindy.R;
import com.aimindy.helper.PrefManager;

/**
 * Created by android on 1/23/2017.
 */

public class FirstWelcomeFragment extends Fragment {

    EditText txt_name;
    private PrefManager prefManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.welcome_slide1, container, false);
        prefManager = new PrefManager(getActivity());

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txt_name = (EditText) view.findViewById(R.id.txt_name);

        txt_name.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (!txt_name.getText().toString().trim().isEmpty()) {
                    prefManager.setName(s.toString());
                }
                else
                {
                    prefManager.setName("Aimindy");

                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });


    }
}
