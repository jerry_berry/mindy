package com.aimindy.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aimindy.AppConstants;
import com.aimindy.MainActivity;
import com.aimindy.R;
import com.aimindy.adapter.EndlessStoreAdapter;
import com.aimindy.helper.ConnectionHelper;
import com.aimindy.helper.NotificationHelper;
import com.aimindy.helper.PrefManager;
import com.aimindy.model.StoreData;
import com.aimindy.model.StoreWrapper;
import com.aimindy.network.ApiFactory;
import com.aimindy.ui.CustomFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by android on 1/24/2017.
 */

public class StoreFragment extends CustomFragment implements EndlessStoreAdapter.NewDataRequest {

    private static final String TAG = "StoreFragment";

    private ListView mList;
    private TextView txt_name,txt_point;
    private TextView mNoItems;
    private ProgressBar mProgress;
    private List<StoreData> mItems;
    private StoreWrapper itemsWrapper;
    private int mCurrentPage = 0;
    private EndlessStoreAdapter mAdapter;
    private boolean mIsNewDataExists = true;
    private Call<StoreWrapper> wCall;
    PrefManager prefManager;
    public StoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_store, container, false);
        prefManager = new PrefManager(getActivity());

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mList = (ListView)view.findViewById(R.id.lst_store);
        txt_name = (TextView) view.findViewById(R.id.txt_name);
        txt_point= (TextView) view.findViewById(R.id.txt_points);
        mNoItems = (TextView)view.findViewById(R.id.no_items);
        mProgress = (ProgressBar)view.findViewById(R.id.progress);
        if (mItems==null){
            loadItems();
        }
        else{
            createAdapter();
        }
    }

    private void createAdapter() {
        mAdapter = new EndlessStoreAdapter(getContext(),mItems,mIsNewDataExists,  this);
        mAdapter.setRunInBackground(false);
        mList.setAdapter(mAdapter);
    }

    private void loadItems() {

        if (!ConnectionHelper.isConnected(getContext())) {
            NotificationHelper.showErrorNoInternet(getSnackAnchor());
            stopLoading();
            return;
        }
        if (wCall!=null&&wCall.isExecuted())
            wCall.cancel();
        wCall = ApiFactory.getApiService().getStore(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("SESSION", "defaultStringIfNothingFound"));

        if (mCurrentPage==0){
            mList.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mNoItems.setVisibility(View.GONE);
        }

        wCall.enqueue(new StoreFragment.CustomCallback<StoreWrapper>(wCall) {
            @Override
            public void onResponse(Call<StoreWrapper> call, Response<StoreWrapper> response) {
                if (isAdded()) {
                    mProgress.setVisibility(View.GONE);
                }

                if (response.isSuccessful()) {
                    itemsWrapper = response.body();
                   if(itemsWrapper.getActivities().isEmpty())
                    {
                        mNoItems.setVisibility(View.VISIBLE);
                        mList.setVisibility(View.GONE);
                    }
                    else {
                       if(prefManager.getName() != null) {
                           txt_name.setText(prefManager.getName());
                       }
                       txt_point.setText(itemsWrapper.getUser_score() + "points");

                        updateData(itemsWrapper.getActivities());
                    }
                }
                else {
                    NotificationHelper.showError(getSnackAnchor(),"1:"+getString(R.string.error_get_items,response.message()));
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<StoreWrapper> call, Throwable t) {
                if (wCall==null||wCall==localCall) {
                    NotificationHelper.showError(getSnackAnchor(),"2:"+getString(R.string.error_get_items,t.getMessage()));
                    stopLoading();
                    if (isAdded()) {
                        mProgress.setVisibility(View.GONE);
                    }
                }
            }
        });
    }



    private abstract class CustomCallback<T> implements Callback<T> {
        protected Call<T> localCall;
        public CustomCallback(Call<T> call){
            localCall = call;
        }
    }


    private double getLatitude() {
        return mListener!=null?mListener.getLatitude():0d;
    }
    private double getLongitude() {
        return mListener!=null?mListener.getLongitude():0d;
    }

    private void updateData(List<StoreData> body) {
        if (body!=null) {
            mIsNewDataExists = body.size()>= AppConstants.PAGE_SIZE;
            if (mAdapter==null||mCurrentPage==0){
                mItems = body;
                createAdapter();
            }
            else{
                mAdapter.onDataReady(body);
            }
            setVisibility();
        }
        else{
            stopLoading();
        }
    }

    private void stopLoading() {
        mIsNewDataExists = false;
        if (mAdapter!=null)
            mAdapter.onDataReady(new ArrayList<StoreData>());
        setVisibility();
    }

    private void setVisibility() {
        if (isAdded()) {
            if (mAdapter == null || mAdapter.getCount() == 0) {
                mNoItems.setVisibility(View.VISIBLE);
                mList.setVisibility(View.GONE);
            }
            else {
                mNoItems.setVisibility(View.GONE);
                mList.setVisibility(View.VISIBLE);
            }
        }
    }


    private void restartLoading() {
        mCurrentPage = 0;
        mAdapter = null;
        loadItems();
    }


    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public boolean needNewData() {
        mCurrentPage++;
        loadItems();
        return mIsNewDataExists;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerLocationListener();
    }

    private void registerLocationListener() {

        IntentFilter filter = new IntentFilter(MainActivity.ACTION_LOCATION_UPDATE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mLocationListener, filter);
    }

    private void unregisterLocationListener() {

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocationListener);
    }

    private BroadcastReceiver mLocationListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!=null&&MainActivity.ACTION_LOCATION_UPDATE.equals(intent.getAction())){
                askRestartWithNewLocation();
            }
        }
    };

    private void askRestartWithNewLocation() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.title_location_updated)
                .setMessage(R.string.msg_location_updated)
                .setNegativeButton(android.R.string.cancel,null)
                .setPositiveButton(R.string.action_load, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restartLoading();
                    }
                })
                .show();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterLocationListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (wCall != null)
            wCall.cancel();
    }


}
