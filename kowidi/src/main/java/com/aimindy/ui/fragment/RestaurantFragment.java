package com.aimindy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.aimindy.R;
import com.aimindy.adapter.RestaurantMenuAdapter;
import com.aimindy.helper.NotificationHelper;
import com.aimindy.model.Item;
import com.aimindy.model.RestaurantResponse;
import com.aimindy.mvp.presenter.RestaurantPresenter;
import com.aimindy.mvp.view.RestaurantView;
import com.aimindy.ui.CustomFragment;
import com.aimindy.widget.FontTextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Date: 01.12.16
 * Time: 21:11
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class RestaurantFragment extends CustomFragment implements RestaurantView {

    private static final String TAG = "RestaurantFragment";


    private static final int LAYOUT = R.layout.fragment_restaurant;


    public static final String EXTRA_RESTARAUNT_ID = "com.clearsfot.kowidi.restaraunt_id";
    public static final String EXTRA_CODE_NAME = "com.clearsfot.kowidi.code_name";


    @InjectPresenter
    RestaurantPresenter presenter;

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.restaurant_address)
    FontTextView restaurantAddress;
    @BindView(R.id.restaurant_website)
    FontTextView restaurantWebsite;
    @BindView(R.id.restaurant_phone)
    FontTextView restaurantPhone;
    @BindView(R.id.items)
    ListView listView;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.progress)
    ProgressBar progress;
   /* @BindView(R.id.row_phone)
    View rowPhone;
    @BindView(R.id.row_site) TableRow siteRow;*/

    private Unbinder unbinder;
    private ImageLoader mLoader;
    private DisplayImageOptions mOptions;



    private RestaurantMenuAdapter itemsAdapter;


    public static RestaurantFragment newInstance(String parentTag, String id, String code) {
        Bundle args = new Bundle();

        args.putString(EXTRA_RESTARAUNT_ID, id);
        args.putString(EXTRA_CODE_NAME, code);

        RestaurantFragment fragment = new RestaurantFragment();
        fragment.setArguments(args);


        //putParentTag(parentTag,args);

        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getArguments().getString(EXTRA_CODE_NAME));
        getActivity().setTitleColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mLoader = ImageLoader.getInstance();
        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .showImageOnFail(R.drawable.default_image)
                .showImageOnLoading(R.drawable.default_image)
                .showImageForEmptyUri(R.drawable.default_image)
                .build();

        String id = getArguments().getString(EXTRA_RESTARAUNT_ID);
        String code = getArguments().getString(EXTRA_CODE_NAME);

        presenter.setRestaurantData(id, code);

    }


    @Override
    public void showErrorAndExit() {
        NotificationHelper.showError(getSnackAnchor(), R.string.error_unknown_id);
        back();
    }

    @Override
    public void setupRestaurantData(RestaurantResponse fullRestaurant) {


        // setup description
        restaurantAddress.setText(getString(R.string.restaurant_address, fullRestaurant.getAddress()));



        Item[] items = fullRestaurant.getItems();
        if (items != null && items.length > 0) {

            itemsAdapter.clear();
            itemsAdapter.addAll(Arrays.asList(items));


        }


        // setup title
      /*  if (fullRestaurant.getName() != null && !TextUtils.isEmpty(fullRestaurant.getName())) {
            ((RestaurantActivity) getActivity()).setTitle(fullRestaurant.getName());
        }*/



        // setup description
       /* if (fullRestaurant.getDescription() == null || TextUtils.isEmpty(fullRestaurant.getDescription())) {
            restDescription.setVisibility(View.GONE);
        } else {
            restDescription.setText(fullRestaurant.getDescription());
        }*/


        //setup photo
        if (fullRestaurant.getPhoto() == null || TextUtils.isEmpty(fullRestaurant.getPhoto())) {
            image.setImageResource(R.drawable.default_image);

        } else {
            mLoader.displayImage(fullRestaurant.getPhoto(), image,mOptions);

        }

        // setup phone
        if (fullRestaurant.getPhone() == null || fullRestaurant.getPhone().length() <= 1) {
            restaurantPhone.setVisibility(View.GONE);
        } else {
            restaurantPhone.setVisibility(View.VISIBLE);
            restaurantPhone.setText(fullRestaurant.getPhone());
        }


        // setup website
        if (fullRestaurant.getWebsite() == null || TextUtils.isEmpty(fullRestaurant.getWebsite())) {
            restaurantWebsite.setVisibility(View.GONE);
        } else {
            restaurantWebsite.setVisibility(View.VISIBLE);
            restaurantWebsite.setText(fullRestaurant.getWebsite());
        }

    }


    @Override
    public void showProgress() {
        toggleProgress(true);
    }

    @Override
    public void hideProgress() {
        toggleProgress(false);
    }


    private void toggleProgress(boolean isProgress) {

        content.setVisibility( isProgress ? View.GONE : View.VISIBLE);
        progress.setVisibility(isProgress ? View.VISIBLE : View.GONE);

    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(LAYOUT, container, false);
        unbinder = ButterKnife.bind(this, view);


        itemsAdapter = new RestaurantMenuAdapter(getContext(), new ArrayList<>());
        listView.setAdapter(itemsAdapter);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


    @Override
    public void showMenuDetail(long id, String code) {
        mListener.showItemDetails(RestaurantFragment.this, id, code);
    }
}
