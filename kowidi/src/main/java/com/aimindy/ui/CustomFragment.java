package com.aimindy.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.arellomobile.mvp.MvpAppCompatFragment;

/** Common methods of the fragment interaction
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2016.
 */
public abstract class CustomFragment extends MvpAppCompatFragment {

    protected OnFragmentInteractionListener mListener;
    private static final String ARG_PARENT_TAG = "arg.parent.tag";

    protected View mSnackAnchor = null;
    private String mParentTag = null;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Get fragment tag
     * @return fragment tag
     */
    public abstract String getFragmentTag();

    /**
     * Get snack anchor view
     * @return snack anchor
     */
    protected View getSnackAnchor(){
        if (mListener!=null)
            return mListener.getSnackAnchor();
        return null;
    }

    /**
     * Hide keyboard
     * @param v view
     */
    protected void hideKeyboard(View v) {
        InputMethodManager imm =
                (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    /**
     * Go to back parent tag fragment
     * @param parentTag parent tag
     */
    protected void back(String parentTag){
        if (mListener!=null)
            mListener.back(parentTag);
    }

    /**
     * Put a string to bundle
     * @param name name of parameter
     * @param value value of parameter
     * @param bundle bundle
     */
    protected static void putStringToBundle(String name,String value, Bundle bundle){
        if (name==null||value==null||bundle==null)
            return;
        bundle.putString(name,value);
    }

    /**
     * Put parent tag to bundle
     * @param parentTag parent tag name
     * @param bundle bundle
     */
    protected static void putParentTag(String parentTag,Bundle bundle){
        putStringToBundle(ARG_PARENT_TAG, parentTag, bundle);
    }

    /**
     * Restore parent tag from bundle
     * @param bundle bundle
     */
    protected void setParentTag(Bundle bundle){
        if (bundle==null)
            return;
        mParentTag = bundle.getString(ARG_PARENT_TAG);
    }

    /**
     * Get parent tag
     * @return parent tag
     */
    protected String getParentTag(){
        return mParentTag;
    }

    /**
     * Go back to parent tag fragment
     */
    protected void back(){
        back(getParentTag());
    }

}
