package com.aimindy.ui;

import android.view.View;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2016.
 */
public interface OnFragmentInteractionListener {
    /**
     * Get snack anchor
     * @return snack anchor
     */
    View getSnackAnchor();

    /**
     * Get current latitude
     * @return latitude
     */
    double getLatitude();

    /**
     * Get current longitude
     * @return longitude
     */
    double getLongitude();

    /**
     * Go back to parent fragment
     * @param parentTag tag of the parent fragment
     */
    void back(String parentTag);

    /**
     * Show item details
     * @param fragment parent fragment
     * @param itemId item id
     * @param itemCodeName item code name
     */
    void showItemDetails(CustomFragment fragment, long itemId, String itemCodeName);


    /**
     * Show item details
     * @param fragment parent fragment
     * @param itemId item id
     * @param itemCodeName item code name
     */
    void showRestaurant(CustomFragment fragment, String itemId, String itemCodeName);
}
