package com.aimindy.ui;

/** Allow fragment to handle back button before activity
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 12.02.2016.
 */
public interface BackButtonHandler {
    /**
     * Back button handler method
     * @return false if back button handled
     */
    boolean onBackButtonPressed();
}
