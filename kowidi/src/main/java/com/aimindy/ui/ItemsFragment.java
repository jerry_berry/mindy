package com.aimindy.ui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.aimindy.AppConstants;
import com.aimindy.MainActivity;
import com.aimindy.R;
import com.aimindy.adapter.EndlessItemsAdapter;
import com.aimindy.helper.ConnectionHelper;
import com.aimindy.helper.NotificationHelper;
import com.aimindy.model.Item;
import com.aimindy.model.ItemWrapper;
import com.aimindy.model.Restoran;
import com.aimindy.mvp.presenter.SuggestionPresenter;
import com.aimindy.mvp.view.SuggestionView;
import com.aimindy.network.ApiFactory;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ItemsFragment extends CustomFragment implements EndlessItemsAdapter.NewDataRequest, SuggestionView, TextToSpeech.OnInitListener  {

    private static final String TAG = "ItemsFragment";
    private String mCurFilter = null;
    private SearchView mSearchView;
    private ListView mList;
    private TextView mNoItems;
    private ProgressBar mProgress;
   // private SearchView auto_search;
    //private TextView txt_refine;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private List<Item> mItems;
    private ItemWrapper itemsWrapper;
    private int mCurrentPage = 0;
    private EndlessItemsAdapter mAdapter;
    private boolean mIsNewDataExists = true;
    private Call<List<Item>> mCall;
    private Call<ItemWrapper> wCall;
    private Gson gson = new Gson();
    private ImageView btnButton;
    boolean speak = false;
    TextToSpeech tts;
    String speakResult;
    MenuItem searchItem;
    AutoCompleteTextView searchTextInside;
    @InjectPresenter
    SuggestionPresenter suggestionPresenter;

    SimpleCursorAdapter suggestionAdapter;



    public ItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        tts = new TextToSpeech(getActivity(), this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_items, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mList = (ListView)view.findViewById(R.id.items);
        btnButton = (ImageView) view. findViewById(R.id.btnButton);
        mSearchView = (SearchView) view.findViewById(R.id.auto_search);

        btnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSearchView.clearFocus();
                Item item = (Item)mAdapter.getItem(position);
                if (item!=null){

                    // This is callback to activity
                    mListener.showItemDetails(ItemsFragment.this, item.itemId, item.itemCodeName);
                }
            }
        });
        mNoItems = (TextView)view.findViewById(R.id.no_items);
        mProgress = (ProgressBar)view.findViewById(R.id.progress);

        searchTextInside = (AutoCompleteTextView) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchTextInside.setThreshold(1);


        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Timber.d("onQueryTextSubmit");

                hideKeyboard(mSearchView);
                speak = false;
                String prevFilter = mCurFilter;
                mCurFilter = !TextUtils.isEmpty(query) ? query : null;
                if (mCurFilter != null && !mCurFilter.equals(prevFilter) || mCurFilter == null && prevFilter != null) {
                    restartLoading();
                }

                return true;

            }

            @Override
            public boolean onQueryTextChange(String query) {
                Timber.d("onQueryTextChange: %s", query);

//                if (query.length() <= 1) {
//                    searchTextInside.showDropDown();
//                }

                suggestionPresenter.suggest(query);
                return false;
            }

        });



        mSearchView.setIconifiedByDefault(false);

        mSearchView.setSubmitButtonEnabled(true);

        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                String prevFilter = mCurFilter;
                mCurFilter = null;
                if (prevFilter != null)
                    restartLoading();
            }
        });

        if (mCurFilter!=null){
           // searchItem.expandActionView();
            mSearchView.setIconified(false);
            mSearchView.setQuery(mCurFilter, false);
            mSearchView.clearFocus();
        }
        else{
           // searchItem.collapseActionView();
            mSearchView.setIconified(true);
        }



        mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                suggestionPresenter.onSuggestionClick(position);
                return false;
            }
        });



        String[] from = {"name", "address"};
        int[] to = {R.id.name_textView, R.id.address_textView};

        suggestionAdapter = new SimpleCursorAdapter(getContext(), R.layout.item_suggestion, null, from, to, -1000);

        mSearchView.setSuggestionsAdapter(suggestionAdapter);
        //txt_refine = (TextView) view.findViewById(R.id.txt_refine);

       /* txt_refine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheet3DialogFragment();
                bottomSheetDialogFragment.show(getFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });*/
        if (mItems==null){
            loadItems();
        }
        else{
            createAdapter();
        }




    }

    private void createAdapter() {
        mAdapter = new EndlessItemsAdapter(getContext(),mItems,mIsNewDataExists,this);
        mAdapter.setRunInBackground(false);
        mList.setAdapter(mAdapter);
    }

    private void loadItems() {

        if (!ConnectionHelper.isConnected(getContext())) {
            NotificationHelper.showErrorNoInternet(getSnackAnchor());
            stopLoading();
            return;
        }

        if (wCall!=null&&wCall.isExecuted())
            wCall.cancel();
        if (mCurFilter==null){
            wCall = ApiFactory.getApiService().getItems(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("SESSION", "defaultStringIfNothingFound"),getLatitude(),getLongitude(),mCurrentPage* AppConstants.PAGE_SIZE);
        }
        else {
            wCall = ApiFactory.getApiService().search(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("SESSION", "defaultStringIfNothingFound"), mCurFilter,0,0,mCurrentPage*AppConstants.PAGE_SIZE,speak);
        }

        if (mCurrentPage==0){
            mList.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mNoItems.setVisibility(View.GONE);
        }

        wCall.enqueue(new CustomCallback<ItemWrapper>(wCall) {
            @Override
            public void onResponse(Call<ItemWrapper> call, Response<ItemWrapper> response) {
                if (isAdded()) {
                    mProgress.setVisibility(View.GONE);
                }

                if (response.isSuccessful()) {
                    itemsWrapper = response.body();
                    updateData(itemsWrapper.getItems());
                    if (itemsWrapper.speak) {
                        speakOut(itemsWrapper.getAnswer());
                    }
                }
                else {
                    NotificationHelper.showError(getSnackAnchor(),"1:"+getString(R.string.error_get_items,response.message()));
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ItemWrapper> call, Throwable t) {
                if (wCall==null||wCall==localCall) {
                    NotificationHelper.showError(getSnackAnchor(),"2:"+getString(R.string.error_get_items,t.getMessage()));
                    stopLoading();
                    if (isAdded()) {
                        mProgress.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                speakOut("");
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }


    }

    private void speakOut(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    private abstract class CustomCallback<T> implements Callback<T> {
        protected Call<T> localCall;
        public CustomCallback(Call<T> call){
            localCall = call;
        }
    }


    private double getLatitude() {
        return mListener!=null?mListener.getLatitude():0d;
    }
    private double getLongitude() {
        return mListener!=null?mListener.getLongitude():0d;
    }

    private void updateData(List<Item> body) {
        if (body!=null) {
            mIsNewDataExists = body.size()>=AppConstants.PAGE_SIZE;
            if (mAdapter==null||mCurrentPage==0){
                mItems = body;
                createAdapter();
            }
            else{
                mAdapter.onDataReady(body);
            }
            setVisibility();
        }
        else{
            stopLoading();
        }
    }

    private void stopLoading() {
        mIsNewDataExists = false;
        if (mAdapter!=null)
            mAdapter.onDataReady(new ArrayList<Item>());
        setVisibility();
    }

    private void setVisibility() {
        if (isAdded()) {
            if (mAdapter == null || mAdapter.getCount() == 0) {
                mNoItems.setVisibility(View.VISIBLE);
                mList.setVisibility(View.GONE);
            }
            else {
                mNoItems.setVisibility(View.GONE);
                mList.setVisibility(View.VISIBLE);
            }
        }
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);

         searchItem = menu.findItem(R.id.action_search);
         mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);


        searchTextInside = (AutoCompleteTextView) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchTextInside.setThreshold(1);


        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Timber.d("onQueryTextSubmit");

                hideKeyboard(mSearchView);
                speak = false;
                String prevFilter = mCurFilter;
                mCurFilter = !TextUtils.isEmpty(query) ? query : null;
                if (mCurFilter != null && !mCurFilter.equals(prevFilter) || mCurFilter == null && prevFilter != null) {
                    restartLoading();
                }

                return true;

            }

            @Override
            public boolean onQueryTextChange(String query) {
                Timber.d("onQueryTextChange: %s", query);

//                if (query.length() <= 1) {
//                    searchTextInside.showDropDown();
//                }

                suggestionPresenter.suggest(query);
                return false;
            }

        });



        mSearchView.setIconifiedByDefault(false);

        mSearchView.setSubmitButtonEnabled(true);

        MenuItemCompat.setOnActionExpandListener(searchItem,new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                String prevFilter = mCurFilter;
                mCurFilter = null;
                if (prevFilter != null)
                    restartLoading();
                return true;
            }
        });

        if (mCurFilter!=null){
            searchItem.expandActionView();
            mSearchView.setIconified(false);
            mSearchView.setQuery(mCurFilter, false);
            mSearchView.clearFocus();
        }
        else{
            searchItem.collapseActionView();
            mSearchView.setIconified(true);
        }



        mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                suggestionPresenter.onSuggestionClick(position);
                return false;
            }
        });



        String[] from = {"name", "address"};
        int[] to = {R.id.name_textView, R.id.address_textView};

        suggestionAdapter = new SimpleCursorAdapter(getContext(), R.layout.item_suggestion, null, from, to, -1000);

        mSearchView.setSuggestionsAdapter(suggestionAdapter);
        super.onCreateOptionsMenu(menu, inflater);
    }
*/
    private void restartLoading() {
        mCurrentPage = 0;
        mAdapter = null;
        loadItems();
    }


    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public boolean needNewData() {
        mCurrentPage++;
        loadItems();
        return mIsNewDataExists;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerLocationListener();
    }

    private void registerLocationListener() {

        IntentFilter filter = new IntentFilter(MainActivity.ACTION_LOCATION_UPDATE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mLocationListener, filter);
    }

    private void unregisterLocationListener() {

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocationListener);
    }

    private BroadcastReceiver mLocationListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!=null&&MainActivity.ACTION_LOCATION_UPDATE.equals(intent.getAction())){
                askRestartWithNewLocation();
            }
        }
    };

    private void askRestartWithNewLocation() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.title_location_updated)
                .setMessage(R.string.msg_location_updated)
                .setNegativeButton(android.R.string.cancel,null)
                .setPositiveButton(R.string.action_load, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restartLoading();
                    }
                })
                .show();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterLocationListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCall!=null)
            mCall.cancel();

        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }


    @Override
    public void setupSuggestions(MatrixCursor cursor) {
        Timber.d("setup new data from cursor: %s", cursor);

        suggestionAdapter.changeCursor(cursor);
        suggestionAdapter.notifyDataSetChanged();
    }

    @Override
    public void openDetailFragment(Restoran restoran) {

        Timber.d("id: %s, code: %s", restoran.getRestaurantId(), restoran.getRestaurantCodeName());
        mListener.showRestaurant(ItemsFragment.this, restoran.getRestaurantId(),
                restoran.getRestaurantCodeName());


        mSearchView.clearFocus();
    }

    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {


                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    speakResult =result.get(0);

                    //speakOut(speakResult);
                     mCurFilter = !TextUtils.isEmpty(result.get(0)) ? result.get(0) : null;
                    if (mCurFilter != null) {
                        //searchItem.expandActionView();
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                       // mSearchView.setQuery(mCurFilter, false);
                        searchTextInside.setText(mCurFilter);
                        mSearchView.clearFocus();
                        mSearchView.setSuggestionsAdapter(null);
                        speak = true;

                        restartLoading();


                    }


                }
                break;
            }

        }
    }

}
