package com.aimindy.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.aimindy.R;
import com.aimindy.helper.ConnectionHelper;
import com.aimindy.helper.NotificationHelper;
import com.aimindy.model.ItemResponse;
import com.aimindy.mvp.presenter.DetailsPresenter;
import com.aimindy.mvp.view.DetailsView;
import com.aimindy.network.ApiFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends CustomFragment implements DetailsView {

    private static final String TAG = "DetailsFragment";

    public static final String ARG_ITEM_ID = "itemId";
    public static final String ARG_CODENAME = "codeName";


    private static final int LAYOUT = R.layout.fragment_details;

    @InjectPresenter
    DetailsPresenter presenter;

    private long mId;
    private String mCodeName;

    private ItemResponse mItem;
    private ViewGroup mLayoutMain;
    private ViewGroup mLayoutProgress;

    private TextView mTitle;
    private TextView mDescription;
    private TextView mPrice;
    private ImageView mImage;
    private ViewGroup mLayoutRestaurant;
    private TextView mRestaurant;
    private ViewGroup mLayoutAddress;
    private TextView mAddress;
    private ViewGroup mLayoutDistance;
    private Button mMap;
    private Button uploadButton;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(String parentTag,long itemId,String codeName) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        //putParentTag(parentTag,args);
        args.putLong(ARG_ITEM_ID, itemId);
        args.putString(ARG_CODENAME, codeName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            setParentTag(getArguments());
            mId = getArguments().getLong(ARG_ITEM_ID,0);
            mCodeName = getArguments().getString(ARG_CODENAME);
        }
        if (mId==0||mCodeName==null){
            NotificationHelper.showError(getSnackAnchor(),R.string.error_unknown_id);
            back();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(LAYOUT, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mTitle = (TextView)view.findViewById(R.id.title);
        mDescription = (TextView)view.findViewById(R.id.description);
        mPrice = (TextView)view.findViewById(R.id.price);
        mImage = (ImageView)view.findViewById(R.id.image);
        mLayoutRestaurant = (ViewGroup)view.findViewById(R.id.layout_restaurant);
        mRestaurant = (TextView)view.findViewById(R.id.restaurant_name);
        mLayoutAddress = (ViewGroup)view.findViewById(R.id.layout_address);
        mAddress = (TextView)view.findViewById(R.id.restaurant_address);
        mLayoutDistance = (ViewGroup)view.findViewById(R.id.layout_map);
        mMap = (Button)view.findViewById(R.id.button_map);
        uploadButton = (Button) view.findViewById(R.id.upload_button);
        mLayoutMain = (ViewGroup)view.findViewById(R.id.layout_main);
        mLayoutProgress = (ViewGroup)view.findViewById(R.id.layout_progress);
        if (mItem==null){
            loadItem();
        }
        else {
            showViews();
        }
    }

    private void showViews() {
        mLayoutMain.setVisibility(View.VISIBLE);
        mLayoutProgress.setVisibility(View.GONE);
        if (mItem.itemName!=null)
            mTitle.setText(mItem.itemName);
        else
            mTitle.setVisibility(View.GONE);

        if (mItem.itemDescription!=null)
            mDescription.setText(mItem.itemDescription);
        else
            mDescription.setVisibility(View.GONE);

        if (mItem.itemPrice!=null){
            mPrice.setText(getString(R.string.price_format, mItem.itemPrice));
        }
        else
            mPrice.setVisibility(View.GONE);

        if (mItem.itemPicture!=null){
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                    .showImageOnFail(R.drawable.default_image)
                    .showImageOnLoading(R.drawable.default_image)
                    .showImageForEmptyUri(R.drawable.default_image)
                    .build();
            ImageLoader.getInstance().displayImage(mItem.itemPicture, mImage, options);

        }
        if (mItem.restaurantName!=null){
            mLayoutRestaurant.setVisibility(View.VISIBLE);
            mRestaurant.setText(mItem.restaurantName);
        }
        else
            mLayoutRestaurant.setVisibility(View.GONE);

        if (mItem.restaurantAddress!=null){
            mLayoutAddress.setVisibility(View.VISIBLE);
            mAddress.setText(mItem.restaurantAddress);
        }
        else
            mLayoutAddress.setVisibility(View.GONE);

        mMap.setVisibility(View.GONE);
        if (mItem.itemDistance!=null||mItem.itemLat!=null&&mItem.itemLng!=null){
            mLayoutDistance.setVisibility(View.VISIBLE);
            if (mItem.itemLat!=null&&mItem.itemLng!=null){
                Uri uri = getRestaurantUri();
                final Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getContext().getPackageManager())!=null) {
                    mMap.setVisibility(View.VISIBLE);
                    mMap.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(mapIntent);
                        }
                    });
                }
            }
        }

        uploadButton.setOnClickListener(view -> presenter.beginUploading(mItem.itemId));
    }

    /**
     * Get uri to show restaurant on the map
     * @return map geo uri
     */
    private Uri getRestaurantUri() {
        StringBuilder sb = new StringBuilder();
        sb.append("geo:0,0?q=");
        sb.append(mItem.itemLat);
        sb.append(",");
        sb.append(mItem.itemLng);
        if (mItem.restaurantName!=null) {
            sb.append("(");
            try {
                sb.append(URLEncoder.encode(mItem.restaurantName,"UTF-8"));
            } catch (UnsupportedEncodingException e) {
                sb.append(getString(R.string.restaurant_map_general));
                e.printStackTrace();
            }
            sb.append(")");
        }
        return Uri.parse(sb.toString());
    }

    /**
     * Load items from api
     */
    private void loadItem() {
        mLayoutMain.setVisibility(View.GONE);
        mLayoutProgress.setVisibility(View.VISIBLE);
        if (!ConnectionHelper.isConnected(getContext())) {
            NotificationHelper.showErrorNoInternet(getSnackAnchor());
            back();
            return;
        }

        Call<ItemResponse> call = ApiFactory.getApiService().getItem(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("SESSION", "defaultStringIfNothingFound"),mId,mCodeName);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(Call<ItemResponse> call, Response<ItemResponse> response) {
                if (response!=null&&response.isSuccessful()&&response.body()!=null){
                    mItem = response.body();
                    if (mItem.isSuccess()){
                        if (isAdded())
                            showViews();
                    }
                    else{
                        NotificationHelper.showError(getSnackAnchor(),getString(R.string.error_get_item,mItem.getError()));
                        back();
                    }
                }
                else{
                    NotificationHelper.showError(getSnackAnchor(),R.string.error_get_item_general);
                    back();
                }
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {
                NotificationHelper.showError(getSnackAnchor(),getString(R.string.error_get_item,t.getMessage()));
                back();
            }
        });
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
