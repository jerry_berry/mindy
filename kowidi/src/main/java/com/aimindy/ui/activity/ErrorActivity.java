package com.aimindy.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.aimindy.R;

/**
 * Date: 29.11.16
 * Time: 11:57
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class ErrorActivity extends AppCompatActivity {



    private static final int LAYOUT = R.layout.activity_error;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
    }
}
