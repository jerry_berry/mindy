package com.aimindy.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.aimindy.R;
import com.aimindy.mvp.presenter.UploadPhotoPresenter;
import com.aimindy.mvp.view.UploadPhotoView;
import com.aimindy.ui.BackButtonHandler;
import com.aimindy.ui.CustomFragment;
import com.aimindy.ui.DetailsFragment;
import com.aimindy.ui.OnFragmentInteractionListener;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import timber.log.Timber;

/**
 * Date: 02.12.16
 * Time: 11:34
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class DetailActivity extends MvpAppCompatActivity implements OnFragmentInteractionListener, UploadPhotoView {


    private static final int LAYOUT = R.layout.activity_item;


    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 101;


    private FragmentManager mFm;
    private View mSnackAnchor;

    private Long itemId;
    private String itemCode;


    @InjectPresenter(type = PresenterType.GLOBAL)
    UploadPhotoPresenter photoPresenter;


    @BindView(R.id.root_layout) CoordinatorLayout rootLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(LAYOUT);
        ButterKnife.bind(this);


        itemId = getIntent().getExtras().getLong(DetailsFragment.ARG_ITEM_ID);
        itemCode = getIntent().getExtras().getString(DetailsFragment.ARG_CODENAME);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mFm = getSupportFragmentManager();

        mSnackAnchor = findViewById(R.id.snackAnchor);
        if (savedInstanceState==null) toMainFragment();

        EasyImage.configuration(this)
                .setImagesFolderName(getString(R.string.app_name))
//                .saveInAppExternalFilesDir()
                .saveInRootPicturesDirectory()
                .setCopyExistingPicturesToPublicLocation(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            onBackPressed();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public View getSnackAnchor() {
        return mSnackAnchor;
    }

    @Override
    public double getLatitude() {
        return 0;
    }

    @Override
    public double getLongitude() {
        return 0;
    }

    @Override
    public void back(String parentTag) {
        revertFragment(parentTag);
    }

    @Override
    public void showItemDetails(CustomFragment fragment, long itemId, String itemCodeName) {

    }

    @Override
    public void showRestaurant(CustomFragment fragment, String itemId, String itemCodeName) {

    }


    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


    @Override
    public void onBackPressed() {
        boolean isBackButtonAllowed = true;
        if (mFm.getBackStackEntryCount()>0) {
            String tag = mFm.getBackStackEntryAt(mFm.getBackStackEntryCount() - 1).getName();
            Fragment f = mFm.findFragmentByTag(tag);
            if (f!=null&&f instanceof BackButtonHandler){
                isBackButtonAllowed = ((BackButtonHandler)f).onBackButtonPressed();
            }
        }
        if (isBackButtonAllowed) {
            if (mFm.getBackStackEntryCount() > 1)
                mFm.popBackStack();
            else
                finish();
        }
    }


    private Fragment revertFragment(String tag) {
        Fragment fr = mFm.findFragmentByTag(tag);
        mFm.popBackStackImmediate(tag, 0);
        return fr;
    }


    private void toMainFragment() {


        DetailsFragment f = DetailsFragment.newInstance("", itemId,itemCode);
        switchFragment(f,f.getFragmentTag(),true,false,false);

    }


    private void switchFragment(CustomFragment fragment, String tag,boolean addToBackStack, boolean clearStack,boolean mainFragment) {

        if (clearStack){
            mFm.popBackStackImmediate(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        FragmentTransaction ft = mFm.beginTransaction();
        ft.setCustomAnimations(mainFragment?0:R.anim.enter, R.anim.exit, R.anim.pop_enter, mainFragment?R.anim.exit:R.anim.pop_exit);
        ft.replace(R.id.container, fragment, tag);
        if (addToBackStack)
            ft.addToBackStack(tag);
        ft.commit();
    }


    @Override
    public void checkPermission(long itemId) {

        // В начале мы должны проверить, есть ли у приложения права на доступ к внешнему хранилищу...
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestWriteExternalStoragePermission();


        } else {

            photoPresenter.openPhotoSelect(itemId);

        }

    }

    @Override
    public void pickImage(long itemId) {
        EasyImage.openChooserWithGallery(this, "Pick source", 0);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUploadingError() {
        showMessage(getString(R.string.error_uploading_photo));
    }

    private void requestWriteExternalStoragePermission() {

        // BEGIN_INCLUDE(storage_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            Snackbar.make(rootLayout, R.string.permission_write_extra_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.OK, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(DetailActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    })
                    .show();

        } else {

            // Storage access permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_EXTERNAL_STORAGE);

        }
        //END_INCLUDE(storage_permission_request

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                if (imageFile != null) {
                    Timber.d("file is: %s", imageFile.getAbsolutePath());
                    photoPresenter.uploadPhoto(imageFile);
                } else {
                    Timber.e("image file is null");
                }

            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(DetailActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });

    }

}
