package com.aimindy.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.aimindy.R;
import com.aimindy.mvp.presenter.LoginPresenter;
import com.aimindy.mvp.view.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Date: 28.11.16
 * Time: 15:38
 *
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class LoginActivity extends MvpAppCompatActivity implements LoginView, DialogInterface.OnCancelListener {


    private static final int LAYOUT = R.layout.activity_login;

    @BindView(R.id.usernameEdit)
    EditText emailView;
    @BindView(R.id.passwordEdit)
    EditText passwordView;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.login_progress) ProgressBar progressView;
    @BindView(R.id.facebook_login_button)
    LoginButton facebookButton;
    @BindView(R.id.link_to_register)
    TextView link_to_register;
    @InjectPresenter
    LoginPresenter presenter;

    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;


    private AlertDialog errorDialog;
    public static LoginActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(LAYOUT);
        activity = this;
        ButterKnife.bind(this);


        errorDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setOnCancelListener(this)
                .create();



        callbackManager = CallbackManager.Factory.create();



        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {


                if (currentAccessToken != null) {
                    Timber.d("catch on accesstoken tracker");
                    presenter.facebookLogin();
                }

            }
        };



        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            Timber.d("check on startup");
            presenter.facebookLogin();
        }

        link_to_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);

                finish();
            }
        });
    }



    @OnClick(R.id.loginButton)
    public void onLoginButtonClick() {


        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();


        presenter.login(email, password);
    }

    @Override
    public void successLogin() {
        Intent intent = new Intent(this, SplashActivtiy.class);
        startActivity(intent);

        finish();
    }

    @Override
    public void showLoginError() {
        Timber.d("need to show login error message");

        Toast.makeText(this, R.string.error_login_in, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProcess() {
        Timber.d("show process");
        toggleProgress(true);
    }

    @Override
    public void hideProcess() {
        Timber.d("hide process");
        toggleProgress(false);
    }

    @Override
    public void showError(String error) {
        errorDialog.setMessage(error);
        errorDialog.show();
    }

    @Override
    public void hideError() {
        errorDialog.cancel();
    }

    @Override
    public void showError(Integer emailError, Integer passwordError) {
        if (emailError != null) {
            emailView.setError(getString(emailError));
        }
        if (passwordError != null) {
            passwordView.setError(getString(passwordError));
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        presenter.onErrorCancel();
    }


    private void toggleProgress(final boolean show) {

        emailView.setEnabled(!show);
        passwordView.setEnabled(!show);

        loginButton.setVisibility(show ? View.GONE : View.VISIBLE);
        progressView.setVisibility(show ? View.VISIBLE : View.GONE);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (accessTokenTracker != null) {
            accessTokenTracker.stopTracking();
        }

    }
}
