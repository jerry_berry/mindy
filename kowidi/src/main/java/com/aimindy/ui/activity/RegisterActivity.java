package com.aimindy.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aimindy.R;
import com.aimindy.helper.ConnectionHelper;
import com.aimindy.model.RegisterResponse;
import com.aimindy.network.ApiFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by android on 1/19/2017.
 */

public class RegisterActivity extends AppCompatActivity{

    private static final int LAYOUT = R.layout.activity_register;

    @BindView(R.id.usernameEdit)
    EditText emailView;
    @BindView(R.id.passwordEdit)
    EditText passwordView;
    @BindView(R.id.RepasswordEdit)
    EditText repasswordEdit;
    @BindView(R.id.registerButton)
    Button registerButton;
    @BindView(R.id.login_progress)
    ProgressBar progressView;

    RegisterActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(LAYOUT);
        ButterKnife.bind(this);
        activity = this;

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Register();
            }
        });
    }

    private void Register() {

        if (!ConnectionHelper.isConnected(getApplicationContext())) {
            Toast.makeText(activity, "Setting Save Successfully", Toast.LENGTH_LONG).show();

            progressView.setVisibility(View.GONE);
            return;
        }

        progressView.setVisibility(View.VISIBLE);
        registerButton.setVisibility(View.GONE);

        Call<RegisterResponse> call = ApiFactory.getApiService().register(emailView.getText().toString().trim(), passwordView.getText().toString().trim(), repasswordEdit.getText().toString().trim());
        call.enqueue(new RegisterActivity.CustomCallback<RegisterResponse>(call) {

            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response)
            {
                progressView.setVisibility(View.GONE);
                if(response.body().getStatus().toLowerCase().equals("success"))
                {
                    Toast.makeText(activity, "Register Successfully", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);

                    finish();
                }
                else
                {
                    registerButton.setVisibility(View.VISIBLE);
                    progressView.setVisibility(View.GONE);
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                registerButton.setVisibility(View.VISIBLE);
                progressView.setVisibility(View.GONE);
            }
        });
    }
    private abstract class CustomCallback<T> implements Callback<T> {
        protected Call<T> localCall;
        public CustomCallback(Call<T> call){
            localCall = call;
        }
    }


}
