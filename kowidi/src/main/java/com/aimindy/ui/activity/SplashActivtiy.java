package com.aimindy.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.aimindy.mvp.presenter.SplashPresenter;
import com.aimindy.mvp.view.SplashView;

import timber.log.Timber;

/**
 * Date: 28.11.16
 * Time: 15:36
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class SplashActivtiy extends MvpAppCompatActivity implements SplashView {

    @InjectPresenter
    SplashPresenter presenter;
    public static SplashActivtiy activtiy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // By default view attaches to presenter in onStart() method,
        // but we attach it manually for earlier check authorization state.
        activtiy = this;
        getMvpDelegate().onAttach();

        presenter.checkIsUserAuthorized();

    }

    @Override
    public void setAuthorized(boolean isAuthorized) {
        Timber.d("user is authorized: %s", isAuthorized);
        startActivityForResult(new Intent(this, isAuthorized ? WelcomeActivity.class : LoginActivity.class), 0);
        finish();
    }

    @Override
    public void showLoginErrorScreen() {
        startActivityForResult(new Intent(this, ErrorActivity.class), 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        finish();
    }

}
