package com.aimindy.mvp.common;

import android.content.SharedPreferences;

/**
 * Date: 28.11.16
 * Time: 16:19
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class TokenDataStore {


    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";

    private static SharedPreferences sharedPreferences;


    public TokenDataStore(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }


    public void setLogin(String login) {
        sharedPreferences.edit().putString(LOGIN, login).apply();
    }

    public String getLogin() {
        return sharedPreferences.getString(LOGIN, "");
    }

    public void setPassword(String password) {
        sharedPreferences.edit().putString(PASSWORD, password).apply();
    }

    public String getPassword() {
        return sharedPreferences.getString(PASSWORD, "");
    }

    public static void clearData() {
        sharedPreferences.edit().remove(LOGIN).remove(PASSWORD).remove("SESSION").apply();
    }

    public void setData(String login) {
        sharedPreferences.edit().putString(LOGIN, login).apply();
    }
}
