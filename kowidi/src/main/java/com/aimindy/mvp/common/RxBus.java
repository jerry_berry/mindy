package com.aimindy.mvp.common;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Date: 28.11.16
 * Time: 11:42
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class RxBus {


    private final Subject<Object> _bus = PublishSubject.create();

    public void send(Object o) {
        _bus.onNext(o);
    }

    public Observable<Object> asObservable() {
        return _bus;
    }

    public boolean hasObservers() {
        return _bus.hasObservers();
    }


    public static class UploadPhotoEvent {
        public  Long itemId;
        public UploadPhotoEvent(Long itemId) {
            this.itemId = itemId;
        }
    }


    public static class MenuTapEvent {
        public Long itemId;
        public String code;

        public MenuTapEvent(Long itemId, String code) {
            this.itemId = itemId;
            this.code = code;
        }
    }

}
