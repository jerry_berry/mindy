package com.aimindy.mvp.common;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.aimindy.entity.User;

/**
 * Date: 28.11.16
 * Time: 17:06
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class UserDataStore {

    private static final String KEY_SERIALIZED_USER = "serialized_user";

    private static SharedPreferences sharedPreferences;
    private Gson gson;

    public UserDataStore(){

    }

    public UserDataStore(SharedPreferences sharedPreferences, Gson gson) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }



    public void createUser(User user) {
        String serializedUser = gson.toJson(user);
        sharedPreferences.edit().putString(KEY_SERIALIZED_USER, serializedUser).apply();
    }

    public static void clearUser() {
        sharedPreferences.edit().remove(KEY_SERIALIZED_USER).apply();
    }

    public User getUser() {
        String serializedUser = sharedPreferences.getString(KEY_SERIALIZED_USER, null);
        if (!TextUtils.isEmpty(serializedUser)) {
            return gson.fromJson(serializedUser, User.class);
        }

        return null;
    }

}
