package com.aimindy.mvp.common;

import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Date: 28.11.16
 * Time: 16:23
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class SessionDataStore {


    private static final String COOKIE = "cookie";
    private static final String SESSION = "session";
    private static final String REAL_COOKIE = "real_cookie";


    private static SharedPreferences sharedPreferences;


    public SessionDataStore(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }



    public static void clearData() {
        sharedPreferences.edit().remove(COOKIE).remove(SESSION).remove(REAL_COOKIE).apply();
    }

    public void setCookie(String cookie) {
        sharedPreferences.edit().putString(COOKIE, cookie).apply();
    }

    public String getCookie() {
        return sharedPreferences.getString(COOKIE, "");
    }


    public void setSession(String session) {
        sharedPreferences.edit().putString(SESSION, session).apply();
    }

    public String getSession() {
        return sharedPreferences.getString(SESSION, "");
    }


    public void setData(String cookie, String session) {
        sharedPreferences.edit().putString(COOKIE, cookie).putString(SESSION, session).apply();
    }


    public void setRealCookies(HashSet<String> cookies) {
        sharedPreferences.edit().putStringSet(REAL_COOKIE, cookies).apply();
    }


    public Set<String> getRealCookes() {
        return sharedPreferences.getStringSet(REAL_COOKIE, new HashSet<>());
    }




}
