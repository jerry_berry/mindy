package com.aimindy.mvp.view;

import android.database.MatrixCursor;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.aimindy.model.Restoran;

/**
 * Date: 01.12.16
 * Time: 17:47
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public interface SuggestionView extends MvpView {


    void setupSuggestions(MatrixCursor cursor);

    @StateStrategyType(SkipStrategy.class)
    void openDetailFragment(Restoran restoran);

}
