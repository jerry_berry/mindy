package com.aimindy.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.aimindy.model.RestaurantResponse;

/**
 * Date: 01.12.16
 * Time: 21:17
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public interface RestaurantView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showErrorAndExit();


    void setupRestaurantData(RestaurantResponse fullRestaurant);


    void showProgress();

    void hideProgress();


    @StateStrategyType(SkipStrategy.class)
    void showMenuDetail(long id, String code);

}
