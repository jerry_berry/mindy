package com.aimindy.mvp.view;

import com.arellomobile.mvp.MvpView;

/**
 * Date: 28.11.16
 * Time: 11:45
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public interface SplashView extends MvpView {

    void setAuthorized(boolean isAuthorized);

    void showLoginErrorScreen();

}
