package com.aimindy.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Date: 28.11.16
 * Time: 15:39
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public interface LoginView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void successLogin();

    void showLoginError();

    void showProcess();

    void hideProcess();

    void showError(String error);

    void hideError();

    void showError(Integer emailError, Integer passwordError);


}
