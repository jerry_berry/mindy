package com.aimindy.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Date: 05.12.16
 * Time: 13:59
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public interface UploadPhotoView extends MvpView {



    @StateStrategyType(SkipStrategy.class)
    void checkPermission(long itemId);



    @StateStrategyType(SkipStrategy.class)
    void pickImage(long itemId);


    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);


    @StateStrategyType(SkipStrategy.class)
    void showUploadingError();
}
