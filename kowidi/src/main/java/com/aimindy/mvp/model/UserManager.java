package com.aimindy.mvp.model;

import android.text.TextUtils;

import com.facebook.AccessToken;
import com.aimindy.di.UserComponent;
import com.aimindy.di.module.UserModule;
import com.aimindy.entity.User;
import com.aimindy.mvp.common.SessionDataStore;
import com.aimindy.mvp.common.TokenDataStore;
import com.aimindy.mvp.common.UserDataStore;
import com.aimindy.network.ApiService;
import com.aimindy.network.LoginResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Date: 28.11.16
 * Time: 11:45
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class UserManager {



    private ApiService apiService;
    private TokenDataStore tokenDataStore;
    private SessionDataStore sessionDataStore;
    private UserDataStore userDataStore;
    private UserComponent.Builder userComponentBuilder;
    private UserComponent userComponent;
    public static User user;
    public UserManager(ApiService apiService, TokenDataStore tokenDataStore,
                       SessionDataStore sessionDataStore, UserDataStore userDataStore,
                       UserComponent.Builder userComponentBuilder) {

        this.apiService = apiService;
        this.tokenDataStore = tokenDataStore;
        this.sessionDataStore = sessionDataStore;
        this.userDataStore = userDataStore;
        this.userComponentBuilder = userComponentBuilder;

    }


    public Observable<LoginResponse> login(String email, String password) {

        // clear session before request
        sessionDataStore.clearData();

        return apiService.login(email, password)
                .doOnNext(loginResponse -> {
                    if (loginResponse != null && loginResponse.getStatus().toLowerCase().equals("success")) {


                           user = loginResponse.getUser();


                           /* AppPrefs.getAppPrefs(this.context).setIsLogin(true);
                            AppPrefs.getAppPrefs(this.context).putUserId(user.getId());
*/
                        // save for future
                        tokenDataStore.setData(email);

                        sessionDataStore.setData(loginResponse.getCookie(), loginResponse.getSession());


//                        String cookie = String.format("JSESSIONID=%s; Path=/; Secure; HttpOnly", loginResponse.getSession());
//                        HashSet<String> cookies = new HashSet<String>();
//                        cookies.add(cookie);
//
//
//                        sessionDataStore.setRealCookies(cookies);


                        // and save user
                        userDataStore.createUser(loginResponse.getUser());

                        startUserSession();


                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }



    public Observable<LoginResponse> facebookLogin(String key) {

        // Clear data before request
        sessionDataStore.clearData();

        return apiService.facebookLogin(key)
                .doOnNext(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse response) throws Exception {


                        if (response != null && response.getStatus() != null
                                && response.getStatus().toLowerCase().equals("success")) {

                            // we no need to use email and password
                            tokenDataStore.clearData();
                            user = response.getUser();
                            sessionDataStore.setData(response.getCookie(), response.getSession());


//                            String cookie = String.format("JSESSIONID=%s; Path=/; Secure; HttpOnly", response.getSession());
//                            HashSet<String> cookies = new HashSet<String>();
//                            cookies.add(cookie);
//
//
//                            sessionDataStore.setRealCookies(cookies);

                            // and save user
                            userDataStore.createUser(response.getUser());


                            startUserSession();


                        } else {

                            // just clear access token
                            //AccessToken.setCurrentAccessToken(null);

                        }


                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }




    private boolean startUserSession() {

        String session = sessionDataStore.getSession();
        String cookie = sessionDataStore.getCookie();

        User user = userDataStore.getUser();


        if (!TextUtils.isEmpty(session) && !TextUtils.isEmpty(cookie) && user != null) {
            Timber.d("session is started: %s", user);
            userComponent = userComponentBuilder.sessionrModule(new UserModule(user)).build();
            return true;
        }

        return false;
    }


    public void closeUserSession() {
        Timber.i("Close session for user: %s", userDataStore.getUser());

        tokenDataStore.clearData();
        sessionDataStore.clearData();
        userDataStore.clearUser();

        userComponent = null;
    }


    public boolean isUserSessionIsStartedOrStartSessionIfPossible() {
        return userComponent != null || startUserSession();
    }


    public boolean isUserSessionIsStarted() {
        return userComponent != null;
    }


    public boolean hasFacebookCredentials() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && !accessToken.isExpired();
    }


    public boolean hasEmailPasswordCredentials() {
        String email = tokenDataStore.getLogin();
        return !TextUtils.isEmpty(email);
    }



    public Observable<LoginResponse> loginWithFacebookCredentials() {

        if (AccessToken.getCurrentAccessToken() != null) {
            return facebookLogin(AccessToken.getCurrentAccessToken().getUserId());
        } else {
            return Observable.empty();
        }
    }

    public Observable<LoginResponse> loginWithEmailPasswordCredentials() {

        String email = tokenDataStore.getLogin();
        String passw = tokenDataStore.getPassword();

        return login(email, passw);

    }

    public UserComponent getUserComponent() {
        return userComponent;
    }
}
