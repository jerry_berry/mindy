package com.aimindy.mvp.presenter;

import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.facebook.AccessToken;
import com.aimindy.KowidiApp;
import com.aimindy.R;
import com.aimindy.mvp.model.UserManager;
import com.aimindy.mvp.view.LoginView;
import com.aimindy.network.LoginResponse;
import com.aimindy.ui.activity.LoginActivity;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Date: 28.11.16
 * Time: 15:40
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@InjectViewState
public class LoginPresenter extends MvpPresenter<LoginView>{

    @Inject
    UserManager userManager;



    public LoginPresenter() {
        KowidiApp.getAppComponent().inject(this);
    }




    public void login(String email, String password) {


        Integer emailError = null;
        Integer passwordError = null;


        getViewState().showError(null, null);


        if (TextUtils.isEmpty(email)) {
            emailError = R.string.error_field_required;
        }

        if (TextUtils.isEmpty(password)) {
            passwordError = R.string.error_field_required;
        }

        if (emailError != null || passwordError != null) {
            getViewState().showError(emailError, passwordError);

            return;
        }



        getViewState().showProcess();
        Timber.d("try to login with email: %s, and password: %s", email, password);


        userManager.login(email, password)
                .subscribe(loginResponse -> {

                    getViewState().hideProcess();
                    Timber.d("server response: %s", loginResponse);

                    if (loginResponse.getStatus().toLowerCase().equals("success")) {
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.activity).edit().putString("SESSION", loginResponse.getSession()).commit();
                        getViewState().successLogin();
                    } else {
                        getViewState().showLoginError();
                    }

                }, throwable -> {
                    getViewState().hideProcess();
                    getViewState().showError(throwable.getMessage());
                });


    }



    public void onErrorCancel() {
        getViewState().hideError();
    }



    public void facebookLogin() {


        getViewState().showProcess();


        userManager.loginWithFacebookCredentials()
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse loginResponse) throws Exception {

                        getViewState().hideProcess();
                        Timber.d("server response: %s", loginResponse);

                        if (loginResponse.getStatus().toLowerCase().equals("success")) {
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.activity).edit().putString("SESSION", loginResponse.getSession()).commit();

                            getViewState().successLogin();
                        } else {


                            // As feathure - logout from facebook
                            AccessToken.setCurrentAccessToken(null);

                            getViewState().showLoginError();
                        }


                    }
                }, throwable -> {
                    getViewState().hideProcess();
                    getViewState().showError(throwable.getMessage());
                });

    }


}
