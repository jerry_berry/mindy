package com.aimindy.mvp.presenter;

import com.arellomobile.mvp.MvpPresenter;
import com.aimindy.KowidiApp;
import com.aimindy.mvp.model.UserManager;
import com.aimindy.mvp.view.SplashView;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Date: 28.11.16
 * Time: 11:47
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class SplashPresenter extends MvpPresenter<SplashView> {

    @Inject
    UserManager userManager;


    public SplashPresenter() {
        KowidiApp.getAppComponent().inject(this);
    }

    public void checkIsUserAuthorized() {

        boolean isLoginIn = userManager.isUserSessionIsStarted();

        if (isLoginIn) {

            Timber.d("hehe, we already login in");
            setViewAuth(true);

        } else if (userManager.hasFacebookCredentials()) {
            setViewAuth(true);
            /*
            Timber.d("try facebook login");

            userManager.loginWithFacebookCredentials()
                    .subscribe(response -> {

                        Timber.d("facebook response: %s", response);

                        if (response != null && response.getStatus() != null &&
                                response.getStatus().toLowerCase().equals("success")) {
                            setViewAuth(true);
                        } else {
                            setViewAuth(false);
                        }
                    }, throwable -> showErrorScreen());

*/

        } else if (userManager.hasEmailPasswordCredentials()) {
            setViewAuth(true);
/*
            Timber.d("try email login");

            userManager.loginWithEmailPasswordCredentials()
                    .subscribe(response -> {
                        if (response != null && response.getStatus() != null &&
                                response.getStatus().toLowerCase().equals("success")) {
                            setViewAuth(true);
                        } else {
                            setViewAuth(false);
                        }
                    }, throwable -> showErrorScreen());
*/

        } else {
            Timber.d("no saved credentials... just open login screen");
            setViewAuth(false);
        }


    }



    private void setViewAuth(boolean isAuth) {
        for (SplashView splashView : getAttachedViews()) {
            splashView.setAuthorized(isAuth);
        }
    }


    private void showErrorScreen() {
        for (SplashView splashView : getAttachedViews()) {
            splashView.showLoginErrorScreen();
        }
    }


}
