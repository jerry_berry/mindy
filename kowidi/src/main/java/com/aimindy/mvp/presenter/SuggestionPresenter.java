package com.aimindy.mvp.presenter;

import android.database.MatrixCursor;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.aimindy.KowidiApp;
import com.aimindy.model.Restoran;
import com.aimindy.mvp.view.SuggestionView;
import com.aimindy.network.ApiService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Date: 01.12.16
 * Time: 17:46
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@InjectViewState
public class SuggestionPresenter extends MvpPresenter<SuggestionView> {


    @Inject
    ApiService apiService;


    List<Restoran> restoranList;

    String[] columnNames = {"_id", "name", "address", "code", "number"};


    boolean isLoading = false;


    Disposable disposable;



    public SuggestionPresenter() {
        KowidiApp.getAppComponent().inject(this);

        restoranList = new ArrayList<>();

    }





    public void suggest(String query) {

        Timber.d("curr loading state: %s", query);

        // skip if loading
        if (isLoading) {
            return;
        }


        // skip if empty query
        if (query == null || TextUtils.isEmpty(query)) return;


        // start loading
        isLoading = true;

        Timber.d("start loading...");


        disposable = apiService.suggest(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(suggestionResponse -> {

                    isLoading = false;
                    Timber.d("server response: %s", suggestionResponse);

                    if (suggestionResponse != null
                            && suggestionResponse.getStatus().toLowerCase().equals("success")
                            && suggestionResponse.getRestaurants() != null) {


                        restoranList.clear();
                        restoranList.addAll(Arrays.asList(suggestionResponse.getRestaurants()));

                        MatrixCursor cursor = convertToCursor(restoranList);
                        getViewState().setupSuggestions(cursor);


                    }

                }, throwable -> {

                    Timber.e(throwable, "error while loading..");

                    isLoading = false;
                    restoranList.clear();

                    MatrixCursor cursor = convertToCursor(restoranList);
                    getViewState().setupSuggestions(cursor);



                });




    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (disposable != null) disposable.dispose();
    }


    // {"_id, name, address, code, number"};
    private MatrixCursor convertToCursor(List<Restoran> feedlyResults) {


        if (feedlyResults != null) {
            Timber.d("convert to cursor: %s", feedlyResults.size());
        } else {
            Timber.d("convert to cursor null result...");
        }


        if (feedlyResults == null || feedlyResults.size() == 0) {
            return null;
        }


        Timber.d("column names length: %s", columnNames.length);
        MatrixCursor cursor = new MatrixCursor(columnNames);


        int i = 0;

        for (Restoran restoran : feedlyResults) {

            String[] temp = new String[5];

            i = i + 1;

            temp[0] = Integer.toString(i);

            temp[1] = restoran.getRestaurantName();
            temp[2] = restoran.getRestaurantAddress();
            temp[3] = restoran.getRestaurantCodeName();
            temp[4] = restoran.getRestaurantId();

            cursor.addRow(temp);
        }


        return cursor;
    }

    public void onSuggestionClick(int position) {
        Timber.d("get click position: %s", position);



        Restoran restoran  = findRestoranByPosition(position);
        if (restoran != null) {
            Timber.d("find, that this is: %s", restoran);

            getViewState().openDetailFragment(restoran);

        }

    }

    private Restoran findRestoranByPosition(int position) {

        try {
            return restoranList.get(position);
        } catch (Exception e) {
            return  null;
        }

    }
}
