package com.aimindy.mvp.presenter;

import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.aimindy.KowidiApp;
import com.aimindy.model.RestaurantResponse;
import com.aimindy.mvp.common.RxBus;
import com.aimindy.mvp.view.RestaurantView;
import com.aimindy.network.ApiService;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.aimindy.KowidiApp.getContext;

/**
 * Date: 01.12.16
 * Time: 21:18
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */
@InjectViewState
public class RestaurantPresenter extends MvpPresenter<RestaurantView> {


    @Inject
    ApiService apiService;


    @Inject
    RxBus _rxBus;

    private String restId;
    private String restCode;


    private boolean isLoading = false;


    RestaurantResponse fullRestaurant;



    CompositeDisposable disposable;



    public RestaurantPresenter() {
        KowidiApp.getAppComponent().inject(this);
        disposable = new CompositeDisposable();


        disposable.add(
                _rxBus.asObservable().subscribe(event -> {

                    if (event instanceof RxBus.MenuTapEvent) {

                        long id = ((RxBus.MenuTapEvent)event).itemId;
                        String code = ((RxBus.MenuTapEvent)event).code;

                        getViewState().showMenuDetail(id, code);

                    }

                })
        );
    }



    public void setRestaurantData(String id, String code) {
        restId = id;
        restCode = code;



        if (restId == null || TextUtils.isEmpty(restId)) {
            getViewState().showErrorAndExit();
        }


        if (restCode == null || TextUtils.isEmpty(restCode)) {
            getViewState().showErrorAndExit();
        }

        if (fullRestaurant == null) {
            loadRestaurantData();
        }

    }


    private void loadRestaurantData() {

        if (isLoading) return;

        isLoading = true;
        getViewState().showProgress();


        disposable.add(
                apiService.getRestaurant(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("SESSION", "defaultStringIfNothingFound"), createQueryString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(restaurantResponse -> {

                    isLoading = false;
                    getViewState().hideProgress();

                    Timber.d("server response: %s", restaurantResponse);

                    if (restaurantResponse != null && restaurantResponse.getStatus().toLowerCase().equals("success")) {

                        fullRestaurant = restaurantResponse;
                        getViewState().setupRestaurantData(fullRestaurant);

                    } else {
                        getViewState().showErrorAndExit();
                    }

                }, throwable -> {
                    Timber.e(throwable);

                    isLoading = false;
                    getViewState().hideProgress();
                    getViewState().showErrorAndExit();
                })
        );

    }



    private String createQueryString() {
        Timber.d("%s-%s", restId, restCode);
        return String.format("%s-%s", restId, restCode);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (disposable != null) {
            disposable.dispose();
        }
    }
}
