package com.aimindy.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.aimindy.KowidiApp;
import com.aimindy.mvp.common.RxBus;
import com.aimindy.mvp.view.DetailsView;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Date: 05.12.16
 * Time: 18:48
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

@InjectViewState
public class DetailsPresenter extends MvpPresenter<DetailsView> {


    @Inject
    RxBus _rxBus;


    public DetailsPresenter() {
        KowidiApp.getAppComponent().inject(this);
    }



    public void beginUploading(long id) {
        Timber.d("user wants upload photo for item: %s", id);
        _rxBus.send(new RxBus.UploadPhotoEvent(id));
    }


}
