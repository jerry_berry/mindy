package com.aimindy.mvp.presenter;

import android.content.Context;
import android.preference.PreferenceManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.aimindy.KowidiApp;
import com.aimindy.mvp.common.RxBus;
import com.aimindy.mvp.view.UploadPhotoView;
import com.aimindy.network.ApiService;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

import static com.aimindy.KowidiApp.getContext;

/**
 * Date: 05.12.16
 * Time: 13:59
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

@InjectViewState
public class UploadPhotoPresenter extends MvpPresenter<UploadPhotoView> {


    @Inject
    RxBus _rxBus;


    @Inject
    ApiService apiService;


    CompositeDisposable disposable;


    long selectedItemId;
    boolean isLoading = false;

    private Context context;



    public UploadPhotoPresenter() {
        KowidiApp.getAppComponent().inject(this);

        disposable = new CompositeDisposable();
        disposable.add(
                _rxBus.asObservable().subscribe(event -> {
                    if (event instanceof RxBus.UploadPhotoEvent) {
                        Timber.d("user ask upload photo for item %s", ((RxBus.UploadPhotoEvent)event).itemId );

                        getViewState().checkPermission(((RxBus.UploadPhotoEvent)event).itemId);
                    }
                })
        );


        Timber.d("Here I am!");
    }


    public void openPhotoSelect(long id) {

        Timber.d("we have all permissions to upload photo");

        selectedItemId = id;
        getViewState().pickImage(id);



    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null) {
            disposable.clear();
        }
    }


    public void uploadPhoto(File file) {


        if (isLoading) return;


        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);



        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", file.getName(), requestFile);



        isLoading = true;



        disposable.add(apiService.uploadPhoto(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("SESSION", "defaultStringIfNothingFound"), selectedItemId, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uploadPhotoResponse -> {

                    Timber.d("server response is: %s", uploadPhotoResponse);

                    isLoading = false;
                    if (uploadPhotoResponse != null && uploadPhotoResponse.getMessage() != null) {
                        getViewState().showMessage(uploadPhotoResponse.getMessage());
                    }


                }, throwable -> {

                    isLoading = false;
                    Timber.e(throwable, "error while uploading photo: %s", throwable.getMessage());

                    getViewState().showUploadingError();

                })
        );



    }

}
