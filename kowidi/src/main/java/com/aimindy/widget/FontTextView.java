package com.aimindy.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.aimindy.R;
import com.aimindy.helper.FontCache;
import com.aimindy.helper.FontName;

public class FontTextView extends TextView {
    public FontTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.FontTextView, defStyle, 0);
        int ff = a.getInt(R.styleable.FontTextView_custom_font, -1);
        Typeface font = null;
        if (ff==0){
            font = FontCache.getInstance().get(FontName.ROBOTO_SLAB_THIN);
        }
        else if (ff==1){
            font = FontCache.getInstance().get(FontName.ROBOTO_SLAB_LIGHT);
        }
        else if (ff==2){
            font = FontCache.getInstance().get(FontName.ROBOTO_SLAB_REGULAR);
        }
        else if (ff==3){
            font = FontCache.getInstance().get(FontName.ROBOTO_SLAB_BOLD);
        }

        a.recycle();
        if (font!=null)
            setTypeface(font);
    }
}
