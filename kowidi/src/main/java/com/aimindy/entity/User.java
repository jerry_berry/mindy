package com.aimindy.entity;

/**
 * Date: 28.11.16
 * Time: 12:21
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class User {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
