package com.aimindy.model;

/**
 * Created by android on 1/5/2017.
 */

public class SettingData {

    String Allergy_No;
    Integer Allergy_image;
    String Allergy_name;

    public SettingData(String allergy_No, Integer allergy_image, String allergy_name) {
        Allergy_No = allergy_No;
        Allergy_image = allergy_image;
        Allergy_name = allergy_name;
    }

    public String getAllergy_No() {
        return Allergy_No;
    }

    public void setAllergy_No(String allergy_No) {
        Allergy_No = allergy_No;
    }

    public Integer getAllergy_image() {
        return Allergy_image;
    }

    public void setAllergy_image(Integer allergy_image) {
        Allergy_image = allergy_image;
    }

    public String getAllergy_name() {
        return Allergy_name;
    }

    public void setAllergy_name(String allergy_name) {
        Allergy_name = allergy_name;
    }
}
