package com.aimindy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 14.02.2016.
 */
public class ItemResponse extends Item {
    public String error;
    @SerializedName("error:")
    public String anotherError;

    /**
     * Is success responded item
     * @return true if success
     */
    public boolean isSuccess(){
        return error==null&&anotherError==null;
    }
    public String getError(){
        if (anotherError==null)
            return error;
        else
            return anotherError;
    }
}
