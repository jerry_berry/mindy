package com.aimindy.model;

/**
 * Date: 01.12.16
 * Time: 17:33
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 *
 *       "restaurantCodeName": "balbir-s-little-india",
 "restaurantAddress": "Koppstraße 10 1160 ",
 "restaurantName": "balbir´s little india",
 "restaurantId": 4682
 *
 */

public class Restoran {

    private String restaurantCodeName;
    private String restaurantAddress;
    private String restaurantName;
    private String restaurantId;




    public String getRestaurantCodeName() {
        return restaurantCodeName;
    }

    public void setRestaurantCodeName(String restaurantCodeName) {
        this.restaurantCodeName = restaurantCodeName;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }
}
