package com.aimindy.model;

/**
 * Date: 01.12.16
 * Time: 17:34
 * Created by pro2on in project jerry_berry-kowidiapp_beta-8455e4f595c3
 */

public class SuggestionResponse {

    private String status;
    private Restoran[] restaurants;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Restoran[] getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(Restoran[] restaurants) {
        this.restaurants = restaurants;
    }
}
