package com.aimindy.model;

import java.util.List;

/**
 * Created by android on 1/31/2017.
 */

public class StoreWrapper {

    public List<StoreData> activities;
    public String user_score;

    public List<StoreData> getActivities() {
        return activities;
    }

    public void setActivities(List<StoreData> activities) {
        this.activities = activities;
    }

    public String getUser_score() {
        return user_score;
    }

    public void setUser_score(String user_score) {
        this.user_score = user_score;
    }
}
