package com.aimindy.model;
import java.util.List;
/**
 * Created by Jeremy on 12/29/2016.
 */

public class ItemWrapper {
    public List<Item> items;
    public String status;
    public boolean speak;
    public String answer;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSpeak() {
        return speak;
    }

    public void setSpeak(boolean speak) {
        this.speak = speak;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
