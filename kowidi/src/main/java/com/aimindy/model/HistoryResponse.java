package com.aimindy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by android on 1/23/2017.
 */

public class HistoryResponse extends History {
    public String error;
    @SerializedName("error:")
    public String anotherError;

    /**
     * Is success responded item
     *
     * @return true if success
     */
    public boolean isSuccess() {
        return error == null && anotherError == null;
    }

    public String getError() {
        if (anotherError == null)
            return error;
        else
            return anotherError;
    }
}