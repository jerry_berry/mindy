package com.aimindy.model;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/** API item
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 11.02.2016.
 */
public class Item {
    public static final Type LIST_TYPE = new TypeToken<ArrayList<Item>>() {}.getType();
    public long itemId;
    public String itemDescription;
    public String itemName;
    public String itemRate;
    public String itemCodeName;
    public String itemOrderUrl;
    public String itemDistance;
    public String itemLat;
    public String itemLng;
    public String itemPicture;
    public String itemPrice;
    public String itemAttributes;
    public int itemFavoriteUserId;
    public int itemRatedByUser;
    public String restaurantRate;
    public String restaurantName;
    public String restaurantDeliveryCost;
    public String restaurantMinimumOrder;
    public String restaurantAddress;
    public String restaurantWebsite;
    public String restaurantPhone;
}
