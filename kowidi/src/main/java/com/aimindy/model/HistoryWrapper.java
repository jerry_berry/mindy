package com.aimindy.model;

import java.util.List;

/**
 * Created by android on 1/23/2017.
 */

public class HistoryWrapper {

    public List<History> items;
    public String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public List<History> getItems() {
        return items;
    }

    public void setItems(List<History> items) {
        this.items = items;
    }
}
