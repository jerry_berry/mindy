package com.aimindy.model;

/**
 * Created by android on 1/24/2017.
 */

public class StoreData {

    private String name;
    private String icon;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
