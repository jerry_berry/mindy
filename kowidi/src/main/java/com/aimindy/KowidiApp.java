package com.aimindy;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.arellomobile.mvp.MvpFacade;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.aimindy.di.AppComponent;
import com.aimindy.di.DaggerAppComponent;
import com.aimindy.di.module.AppModule;
import com.aimindy.di.module.NetworkModule;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;


/** Application class. It inits all app depended objects
 * Created by Alexey Rogovoy (lexapublic@gmail.com) on 11.02.2016.
 */
public class KowidiApp extends Application {

    private static Context context;


    private static AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();


        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();


        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);


        // Plant Timber instance
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        Timber.plant(new CrashlyticsTree());


        // init storage for moxy presenters
        MvpFacade.init();


        initImageLoader();


        // resolve dagger 2 dependencies
        resolveDependencies();


        // Facebook init
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


    }

    private void resolveDependencies() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    /**
     * Init image loader
     */
    private void initImageLoader() {
        ImageLoader imageLoader = ImageLoader.getInstance();


        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();


        ImageLoaderConfiguration.Builder configBuilder = new ImageLoaderConfiguration.Builder(getApplicationContext());
        configBuilder.defaultDisplayImageOptions(defaultOptions);
        configBuilder.memoryCache(new WeakMemoryCache());
        configBuilder.diskCacheSize(200 * 1024 * 1024);
        configBuilder.diskCacheFileNameGenerator(new HashCodeFileNameGenerator());
        ImageLoaderConfiguration config=configBuilder.build();
        imageLoader.init(config);
    }



    public static Context getContext()
    {
        return context;
    }


    public static AppComponent getAppComponent() {
        return appComponent;
    }


    public class CrashlyticsTree extends Timber.Tree {
        private static final String CRASHLYTICS_KEY_PRIORITY = "priority";
        private static final String CRASHLYTICS_KEY_TAG = "tag";
        private static final String CRASHLYTICS_KEY_MESSAGE = "message";

        @Override
        protected void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
                return;
            }

            Crashlytics.setInt(CRASHLYTICS_KEY_PRIORITY, priority);
            Crashlytics.setString(CRASHLYTICS_KEY_TAG, tag);
            Crashlytics.setString(CRASHLYTICS_KEY_MESSAGE, message);

            if (t == null) {
                Crashlytics.logException(new Exception(message));
            } else {
                Crashlytics.logException(t);
            }
        }
    }

}
